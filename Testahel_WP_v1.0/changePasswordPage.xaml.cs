﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;

namespace Testahel_WP_v1._0
{
    public partial class changePasswordPage : PhoneApplicationPage
    {
        methods methods = new methods();
        public changePasswordPage()
        {
            InitializeComponent();
            tbWel.Text += methods.userName;
            methods.setDateValue(txtDate);
        }

        private void m_oldPassword_PasswordChanged_1(object sender, RoutedEventArgs e)
        {
                if (oldPassword.Password == "")
                    methods.hideEye(btn_showOldPass);
                else
                    methods.showEye(btn_showOldPass);

                if (newPassword.Password == "")
                    methods.hideEye(btn_showNewPass);
                else
                    methods.showEye(btn_showNewPass);
        }

        private void btn_showOldPass_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
                methods.showPassword(oldPassword, showOldPass);
        }

        private void btn_showOldPass_MouseLeave_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
                methods.hidePass(oldPassword, showOldPass);

        }

        private void btn_showNewPass_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
                methods.showPassword(newPassword, showNewPass);
        }

        private void btn_showNewPass_MouseLeave_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
                methods.hidePass(newPassword, showNewPass);
        }

        private async void btnSendData_Click_1(object sender, EventArgs e)
        {
            if (methods.checkNetworkConnection())
            {
                string userName = "";
                if (methods.loginData.RegistrationTypeId == 3)
                {
                    userName = methods.businessUser.IdentityBuisinessNumber.ToString();
                }
                else
                {
                    userName = methods.individualUser.IdentityNumber.ToString();
                }
                string Data = "<ChangePassword xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService\"><UserToken>" + methods.loginData.UserToken + "</UserToken><currentPass>" + oldPassword.Password + "</currentPass><newPass>" + newPassword.Password + "</newPass><userName>" + userName + "</userName></ChangePassword>";
                string ContentType = "application/xml";
                string url = methods.serverURL + "ChangePassword";
                string result = await methods.PostDataToServer(Data, url, ContentType);
                var res = JsonConvert.DeserializeObject<postResult>(result);
                if (res.AffectedRows != 0)
                {
                    MessageBox.Show("لقد تم تغيير كلمة المرور بنجاح ");
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                }
                else if (res.AffectedRows == 0 && res.StatusCode == 200)
                {
                    MessageBox.Show("من فضلك تأكد من كلمة المرور القديمة");
                }
                else
                {
                    MessageBox.Show("عفوا يوجد خطأ ما يرجى المحاولة فى وقتا لاحق");
                }
            }
            else
            {
                MessageBox.Show(methods.internetConnectionFailedMessage);
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }
    }
}