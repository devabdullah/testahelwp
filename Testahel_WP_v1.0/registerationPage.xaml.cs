﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Threading;
using Microsoft.Phone.Data.Linq;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Tasks;
using Newtonsoft.Json;

namespace Testahel_WP_v1._0
{
    public partial class registerationPage : PhoneApplicationPage
    {
        methods methods = new methods();
        BitmapImage bitmapImage;
        DataBaseContext db;
        UmAlQuraCalendar hijriCalendar = new UmAlQuraCalendar();
        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        public registerationPage()
        {
            InitializeComponent();
            fillPickers();
        }

        private void userTypes_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (userTypes.SelectedIndex==0 || userTypes.SelectedIndex==1)
            {
                if (userTypes.SelectedIndex==0)
                {
                    type.Text = "رقم السجل المدني";
                    stk_date.Visibility = Visibility.Visible;
                    stk_msdr.Visibility = Visibility.Visible;
                    stk_num.Visibility = Visibility.Visible;
                }
                else
                {
                    type.Text = "رقم الإقامة";
                    stk_date.Visibility = Visibility.Collapsed;
                    stk_msdr.Visibility = Visibility.Collapsed;
                    stk_num.Visibility = Visibility.Collapsed;
                }
                m6n.Visibility = Visibility.Visible;
                keta3A3mal.Visibility = Visibility.Collapsed;
            }
            else if (userTypes.SelectedIndex==2)
            {
                m6n.Visibility = Visibility.Collapsed;
                keta3A3mal.Visibility = Visibility.Visible;
            }
        }
        public bool validate()
        {
            if (userTypes.SelectedIndex == 2)
            {
                bool txtNo_isEmpty = methods.isEmpty(identityBuissenessNumber.Name,this);
                bool txtName_isEmpty = methods.isEmpty(Name.Name, this);
                
                //bool txtDate_isEmpty = isEmpty(date.Name, req_date.Name);
                bool txtMobNum2_isEmpty = methods.isEmpty(mobNum2.Name, this);
                bool txtEmail2_isEmpty = methods.isEmpty(email2.Name, this);
                //bool txtPass_isEmpty = isEmpty(pass.Name, req_pass.Name);

                bool txtEmail2_rightFormat = validateEmail(email2.Text);
                if (!txtEmail2_isEmpty)
                {
                    if (!txtEmail2_rightFormat)
                    {
                        req_email2.Text = "البريد الإلكترونى غير صحيح";
                    }
                }
                

                if (!txtNo_isEmpty && !txtName_isEmpty && !txtMobNum2_isEmpty && !txtEmail2_isEmpty && txtEmail2_rightFormat)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                bool txtNum_isEmpty = methods.isEmpty(identityNumber.Name, this);
                bool txtFName_isEmpty = methods.isEmpty(fName.Name, this);
                bool txtSName_isEmpty = methods.isEmpty(sName.Name, this);
                bool txtThName_isEmpty = methods.isEmpty(thName.Name, this);
                bool txtLName_isEmpty = methods.isEmpty(lName.Name, this);
                //bool txtDateOfBirth_isEmpty = isEmpty(dateOfBirth.Name, req_dateOfBirth.Name);
                bool txtMobNum_isEmpty = methods.isEmpty(mobNum.Name, this);
                bool txtPhoneNum_isEmpty = methods.isEmpty(phoneNum.Name, this);
                bool txtEmail_isEmpty = methods.isEmpty(email.Name, this);
                bool hafezaNumber_isEmpty = methods.isEmpty(hafezaNumber.Name, this);
                //bool txtMPass_isEmpty = isEmpty(m_pass.Name, req_m_pass.Name);

                bool txtEmail_rightFormat = validateEmail(email.Text);
                if (userTypes.SelectedIndex == 0)
                {
                    if (!txtEmail_isEmpty)
                    {
                        if (!txtEmail_rightFormat)
                        {
                            MessageBox.Show("البريد الإلكترونى غير صحيح");
                        }
                    }


                    if (!txtNum_isEmpty && !hafezaNumber_isEmpty && !txtFName_isEmpty && !txtSName_isEmpty && !txtThName_isEmpty && !txtLName_isEmpty && !txtMobNum_isEmpty && !txtPhoneNum_isEmpty && !txtEmail_isEmpty && txtEmail_rightFormat)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (!txtEmail_isEmpty)
                    {
                        if (!txtEmail_rightFormat)
                        {
                            MessageBox.Show("البريد الإلكترونى غير صحيح");
                        }
                    }


                    if (!txtNum_isEmpty && !txtFName_isEmpty && !txtSName_isEmpty && !txtThName_isEmpty && !txtLName_isEmpty && !txtMobNum_isEmpty && !txtPhoneNum_isEmpty && !txtEmail_isEmpty && txtEmail_rightFormat)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                
                
            }
            
            
        }
        
        public static bool validateEmail(string email)
        {
            return Regex.IsMatch(email, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            if (methods.checkNetworkConnection())
            {
                bool validation = validate();

                if (validation)
                {
                    SystemTray.ProgressIndicator = new ProgressIndicator();
                    methods.setProgressIndicator(true);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ProgressIndicator.Text = "جار التسجيل";
                    WebClient client = new WebClient();
                    client.Headers[HttpRequestHeader.ContentType] = "application/xml";
                    string url = "", data = "";
                    if (userTypes.SelectedIndex == 0 || userTypes.SelectedIndex == 1)
                    {
                        string birthDate = ((DateTime)dateOfBirth.Value).Date.ToString("yyyyMMdd");

                        string hafezaDate = ((DateTime)dateOfHafeza.Value).Date.ToString("yyyyMMdd");

                        cities hafezaSourcePlace = lstMsdrHafeza.SelectedItem as cities;
                        string hafezaIssueSourcePlace = hafezaSourcePlace.Title;
                        string hafezaIssueSourcePlaceID = hafezaSourcePlace.Id.ToString();

                        maritalStatus maritalStatus = lstMaritalStatus.SelectedItem as maritalStatus;
                        string maritalStatusId = maritalStatus.Id.ToString();
                        if (userTypes.SelectedIndex == 0)
                        {
                            url = methods.serverURL + "InsertNewCitizen";
                            data = "<CitizenIndividualsData xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><BirthDateHijri>" + birthDate + "</BirthDateHijri><BirthDateHijriString>String content</BirthDateHijriString><BirthPlace>" + birthPlace.Text + "</BirthPlace><Email>" + email.Text + "</Email><FamilyName>" + lName.Text + "</FamilyName><FirstName>" + fName.Text + "</FirstName><HefezaIssueDateHijri>" + hafezaDate + "</HefezaIssueDateHijri><HefezaIssueSourcePlace>" + hafezaIssueSourcePlace + "</HefezaIssueSourcePlace><HefezaIssueSourcePlaceId>" + hafezaIssueSourcePlaceID + "</HefezaIssueSourcePlaceId><HefezaNumber>" + hafezaNumber.Text + "</HefezaNumber><IdentityNumber>" + identityNumber.Text + "</IdentityNumber><MaritalStatusId>" + maritalStatusId + "</MaritalStatusId><Mobile>" + mobNum.Text + "</Mobile><POBox>" + poBox.Text + "</POBox><Password>" + hidePass.Password + "</Password><SecondName>" + sName.Text + "</SecondName><Telephone>" + phoneNum.Text + "</Telephone><ThirdName>" + thName.Text + "</ThirdName></CitizenIndividualsData>";

                        }
                        else
                        {
                            url = methods.serverURL + "InsertNewForeign";
                            data = "<ForeignIndividualsData xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><BirthDateHijri>" + birthDate + "</BirthDateHijri><BirthDateHijriString>String content</BirthDateHijriString><BirthPlace>" + birthPlace.Text + "</BirthPlace><Email>" + email.Text + "</Email><FamilyName>" + lName.Text + "</FamilyName><FirstName>" + fName.Text + "</FirstName><IdentityNumber>" + identityNumber.Text + "</IdentityNumber><MaritalStatusId>" + maritalStatusId + "</MaritalStatusId><Mobile>" + mobNum.Text + "</Mobile><POBox>" + poBox.Text + "</POBox><Password>" + hidePass.Password + "</Password><SecondName>" + sName.Text + "</SecondName><Telephone>" + phoneNum.Text + "</Telephone><ThirdName>" + thName.Text + "</ThirdName></ForeignIndividualsData>";
                        }
                    }

                    else
                    {
                        banks bank = lstpBanks.SelectedItem as banks;
                        string bankID = bank.BankID.ToString();

                        string commercialDate = ((DateTime)dateOfSglTogary.Value).Date.ToString("yyyyMMdd");

                        cities commercialRegSourcePlace = lstp_cities.SelectedItem as cities;
                        string commercialSourcePlace = commercialRegSourcePlace.Title;
                        string commercialSourcePlaceID = commercialRegSourcePlace.Id.ToString();

                        string attachementImage;
                        byte[] imageArray;
                        if (attachements.Source == null)
                        {
                            attachementImage = string.Empty;
                            imageArray = new byte[0];
                        }
                        else
                        {
                            imageArray = methods.ImageToArray(bitmapImage);
                            attachementImage = Convert.ToBase64String(imageArray);
                        }
                        url = methods.serverURL + "InsertNewBuisinessSector";
                        //url = "http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/InsertNewBuisinessSector";
                        data = "<BuisinessSectorData xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Address>" + address.Text + "</Address><AttachmentImg>" + attachementImage + "</AttachmentImg><BankAccountNo>" + bankNum.Text + "</BankAccountNo><BankId>" + bankID + "</BankId><CommercialRegDateHijri>" + commercialDate + "</CommercialRegDateHijri><CommercialRegSourcePlace>" + commercialSourcePlace + "</CommercialRegSourcePlace><CommercialRegSourcePlaceId>" + commercialSourcePlaceID + "</CommercialRegSourcePlaceId><Email>" + email2.Text + "</Email><Fax>" + fax.Text + "</Fax><IdentityBuisinessNumber>" + identityBuissenessNumber.Text + "</IdentityBuisinessNumber><Mobile>" + mobNum2.Text + "</Mobile><Name>" + Name.Text + "</Name><POBox>" + buisenessPoBox.Text + "</POBox><Password>" + hidePass_k.Password + "</Password><Telephone>" + phoneNum2.Text + "</Telephone><ZipCode>" + zipCode.Text + "</ZipCode></BuisinessSectorData>";
                    }
                    string result = await methods.PostDataToServer(data, url, "application/xml");
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    if (result != "" && !result.Contains("error"))
                    {
                        var r = JsonConvert.DeserializeObject<postResult>(result);
                        if (r.AffectedRows == 1)
                        {
                            MessageBox.Show(r.StatusMessage);
                            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                        }
                        else
                        {
                            MessageBox.Show(r.StatusMessage);
                        }
                    }
                    else
                    {
                        MessageBox.Show(methods.errorMessage);
                    }

                }
                else
                {
                    MessageBox.Show("من فضلك أكمل البيانات المطلوبه");
                }
            }
            else
            {
                MessageBox.Show(methods.internetConnectionFailedMessage);
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void hidePass_PasswordChanged_1(object sender, RoutedEventArgs e)
        {
            if (userTypes.SelectedIndex == 2)
            {
                if (hidePass_k.Password=="")
                {
                    methods.hideEye(btn_showPass_k);
                }
                else
                {
                    methods.showEye(btn_showPass_k);
                }
            }
            else
            {
                if (hidePass.Password == "")
                {
                    methods.hideEye(btn_showPass);
                }
                else
                {
                    methods.showEye(btn_showPass);
                }
            }
            
            
        }

        private void showPass_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void btn_showPass_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (userTypes.SelectedIndex==2)
            {
                methods.showPassword(hidePass_k, showPass_k);
            }
            else
            {
                methods.showPassword(hidePass, showPass);
            }
           
        }

        private void btn_showPass_MouseLeave_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (userTypes.SelectedIndex == 2)
            {
                methods.hidePass(hidePass_k, showPass_k);
            }
            else
            {
                methods.hidePass(hidePass, showPass);
            }
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            
        }

        private async void fillPickers()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            try
            {
                var mS = db.MaritalStatus.FirstOrDefault();
            }
            catch (Exception ex)
            {

                try
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<maritalStatus>();
                        databaseSchemaUpdater.Execute();
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                catch (Exception exc)
                {

                    MessageBox.Show(exc.Message);
                }
            }
            // var allMaritalStatus = db.MaritalStatus.ToList();
            var allMaritalStatus = await methods.getAllMaritalStatus();

            lstMaritalStatus.ItemsSource = allMaritalStatus;
            if (allMaritalStatus != null)
                lstMaritalStatus.SelectedIndex = 0;

            var allCities = await methods.getAllCities();
            lstp_cities.ItemsSource = allCities;
            if (lstp_cities.ItemsSource != null)
                lstp_cities.SelectedIndex = 0;
            lstMsdrHafeza.ItemsSource = allCities;
            if (lstMsdrHafeza.ItemsSource != null)
                lstMsdrHafeza.SelectedIndex = 0;


            var allBanks = await methods.getAllBanks();
            lstpBanks.ItemsSource = allBanks;
            if (lstpBanks.ItemsSource != null)
                lstpBanks.SelectedIndex = 0;
        }

        private void btnAttachement_Click_1(object sender, RoutedEventArgs e)
        {
            PhotoChooserTask photo = new PhotoChooserTask();
            photo.ShowCamera = true;
            photo.Completed += photo_Completed;
            photo.Show();
        }

        void photo_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                var img = new BitmapImage();
                img.SetSource(e.ChosenPhoto);
                bitmapImage = img;
                attachements.Source = img;
            }
        }
    }
}