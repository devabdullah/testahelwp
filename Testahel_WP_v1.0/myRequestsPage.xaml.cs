﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using OfflinePostTask;
using System.Windows.Media;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class myRequestsPage : PhoneApplicationPage
    {
        DataBaseContext db;
        methods methods;// = new methods();
        
        public myRequestsPage()
        {
            InitializeComponent();
            methods = new methods();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            getRequests(); 
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {
            
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains("addRequest"))
            {
                if (settings["addRequest"] == "true")
                {
                    getRequests();
                    settings["addRequest"] = "false";
                    settings.Save();
                }
            }
            
            
        }

        private void llsMyRequests_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            myRequests mine = llsMyRequests.SelectedItem as myRequests;
            if (mine!=null)
            {
                PhoneApplicationService.Current.State["myRequest"] = mine;
            NavigationService.Navigate(new Uri("/myRequestDetailsPage.xaml", UriKind.Relative));
            }           
        }
        private async void getRequests()
        {
                myRequests[] myRequests;
                SystemTray.ProgressIndicator = new ProgressIndicator();
                methods.setProgressIndicator(true);
                SystemTray.ProgressIndicator.Text = "يتم تحميل طلباتي";
                SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                methods = new methods();
                db = new DataBaseContext(DataBaseContext.DBConnectionString);

                myRequests = await methods.getAllMyRequests(methods.userID);
                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                if (myRequests==null)
                {

                    myRequests = db.MyRequests.ToArray();
                    if (myRequests.Count() == 0)
                    {
                        llsMyRequests.Visibility = Visibility.Collapsed;
                        lblSpace.Visibility = Visibility.Visible;
                        lblMyRequests.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    foreach (var request in myRequests)
                    {
                        if (request.RequestStatus == "1")
                        {
                            request.RequestStatus = "مقبول";
                        }
                        else if (request.RequestStatus == "0")
                        {
                            request.RequestStatus = "مرفوض";
                        }
                        else
                            request.RequestStatus = "لم يتم النظر فيه";
                    }
                }
                llsMyRequests.ItemsSource = myRequests;
                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
            
        }

    }
}