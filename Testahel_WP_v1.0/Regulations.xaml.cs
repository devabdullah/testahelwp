﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class Regulations : PhoneApplicationPage
    {
        List<string> FilesNames = new List<string>();
        List<string> FilesDescriptions = new List<string>();
        List<string> Links = new List<string>();
        public class FillingData
        {
            public string FileName { get; set; }
            public string FileDescription { get; set; }
            public string DownloadImgPath { get; set; }
            public string PdfImgPath { get; set; }
            public FillingData(string _FileName, string _DownloadImgPath, string _PdfImgPath, string _FileDescription)
            {
                this.FileName = _FileName;
                this.DownloadImgPath = _DownloadImgPath;
                this.PdfImgPath = _PdfImgPath;
                this.FileDescription = _FileDescription;
            }
        }
        ObservableCollection<RegulationsData> DataList = new ObservableCollection<RegulationsData>();
        RegulationsData[] AllRegulations;
        methods methods = new methods();
        public Regulations()
        {
            InitializeComponent();
            this.Loaded += Regulations_Loaded;
            LLS.SelectionChanged += LLS_SelectionChanged;
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        void LLS_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            RegulationsData fill = LLS.SelectedItem as RegulationsData;
            PhoneApplicationService.Current.State["RegulationData"] = fill;
            NavigationService.Navigate(new Uri("/ShowregulationData.xaml", UriKind.RelativeOrAbsolute));
        }

        async void Regulations_Loaded(object sender, RoutedEventArgs e)
        {
            //WebClient client = new WebClient();
            //client.DownloadStringAsync(new Uri("http://najran.qadirgroup.net/qadirgroup.net/najran/MobileService.svc/GetAllRegulations", UriKind.RelativeOrAbsolute));
            //client.DownloadStringCompleted += client_DownloadStringCompleted;

            if (LLS.ItemsSource == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                methods.setProgressIndicator(true);
                SystemTray.ProgressIndicator.Text = "يتم تحميل اللوائح والإشتراطات";
                SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
            }
            AllRegulations = await methods.GetAllRegulations();
            if (AllRegulations != null)
            {
                DataList.Clear();
                for (int i = 0; i < AllRegulations.Length; i++)
                {
                    RegulationsData RetrievedData = new RegulationsData();
                    RetrievedData.ID = AllRegulations[i].ID;
                    RetrievedData.Name = AllRegulations[i].Name;
                    if (RetrievedData.Name.Length > 25)
                    {
                        RetrievedData.displayedName = RetrievedData.Name.Substring(0, 25) + "...";
                    }
                    else
                        RetrievedData.displayedName = RetrievedData.Name;
                    RetrievedData.Link = AllRegulations[i].Link;
                    RetrievedData.Description = AllRegulations[i].Description;
                    if (RetrievedData.Description.Length > 25)
                    {
                        RetrievedData.displayedDiscription = RetrievedData.Description.Substring(0, 25) + "...";
                    }
                    else
                        RetrievedData.displayedDiscription = RetrievedData.Description;
                    RetrievedData.DownloadImgPath = "/Assets/Icons/Download.png";
                    RetrievedData.PdfImgPath = "/Assets/Icons/pdfIco.png";
                    DataList.Add(RetrievedData);
                }
                LLS.ItemsSource = DataList;
                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
            }
            else
            {
                LLS.Visibility = Visibility.Collapsed;
                internetErrorMsg.Visibility = Visibility.Visible;
                internetErrorMsg.Text = methods.internetConnectionFailedMessage;
                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
            }

        }

        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                string response = e.Result.ToString();
                JArray a = JArray.Parse(response);
                foreach (JObject o in a.Children<JObject>())
                {
                    foreach (JProperty p in o.Properties())
                    {
                        string name = p.Name;
                        string value = p.Value.ToString();
                        if (name.ToLower() == "description")
                        {
                            FilesDescriptions.Add(value);
                        }
                        else if (name.ToLower() == "name")
                        {
                            FilesNames.Add(value);
                        }
                    }
                }
            }
        }
    }
}