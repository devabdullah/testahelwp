﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Windows.Devices.Geolocation;
using System.Device.Location;
using System.Windows.Shapes;
using Microsoft.Phone.Maps.Controls;
using System.Windows.Data;
using System.Text;
using Microsoft.Phone.Net.NetworkInformation;
using Microsoft.Phone.Data.Linq;
using Newtonsoft.Json;
using Microsoft.Phone.Maps.Toolkit;

namespace Testahel_WP_v1._0
{
    public partial class r940Page : PhoneApplicationPage
    {
        #region Global Variables
        DataBaseContext db;
        districts[] allDistricts;
        allRoads[] roads;
        incidentCategories[] allIncidentCategories;
        allIncidentTypes[] incidentTypes;
        methods methods = new methods();
        long DeptID;//الـ أي دي بتاع نوع الفئه زي ما مكتوب فى الويب سيرفيس
        long DistrictID, RoadID, IncidentCategoryType;
        string DistrictName, RoadName;
        bool IsLocationConfirmed = false;
        #endregion

        public r940Page()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            ShowMyLocationOnTheMap();
            codeInConstructor();

        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private async void ShowMyLocationOnTheMap()
        {
            // Get my current location.
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            GeoCoordinate myGeoCoordinate = CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);
            // Make my current location the center of the Map.
            this.mapWithMyLocation.Center = myGeoCoordinate;
            this.mapWithMyLocation.ZoomLevel = 13;
            // Create a small circle to mark the current location.
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Color.FromArgb(255,130,26,24));
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;
            // Create a MapOverlay to contain the circle.
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;
            // Create a MapLayer to contain the MapOverlay.
            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);
            // Add the MapLayer to the Map.
            mapWithMyLocation.Layers.Add(myLocationLayer);

        }

        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            methods.setProgressIndicator(true);
            SystemTray.ProgressIndicator.Text = "يتم إرسال البلاغ";
            SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
            SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
            string RID = "";
            double[] Coordinates = await methods.GetLongitudeLatitude();
            if (methods.checkNetworkConnection())
            {
                    if (RoadID != -1)
                    {
                        RID = RoadID.ToString();
                    }
                    else
                    {
                        RID = "0";
                    }
                    string data = "<IncidentArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Incidents><Incident><CallerNumber>" + mobNum.Text + "</CallerNumber><DeptID>" + DeptID + "</DeptID><DistrictID>" + DistrictID + "</DistrictID><DistrictName>" + DistrictName + "</DistrictName><IncidentDescription>" + req_description.Text + "</IncidentDescription><Latitude>" + Coordinates[1] + "</Latitude><LocationDescription>" + description.Text + "</LocationDescription><Longitude>" + Coordinates[0] + "</Longitude><NationalID>123</NationalID><RoadID>+" + RID + "</RoadID><RoadName>" + RoadName + "</RoadName><TypeID>" + IncidentCategoryType + "</TypeID></Incident></Incidents></IncidentArray>";
                    string url = methods.serverURL + "InsertIncident";
                    if (IsLocationConfirmed)
                    {
                        string result = await methods.PostDataToServer(data, url, "application/xml");
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                        if (result != "")
                        {
                            var r = JsonConvert.DeserializeObject<postResult>(result);
                            if (r.StatusCode == 200)
                            {
                                MessageBox.Show("تم إرسال البلاغ بنجاح");
                                NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.Relative));
                            }
                            else
                            {
                                MessageBox.Show(r.StatusMessage);
                            }
                        }
                        else
                        {
                            MessageBox.Show(methods.errorMessage);
                        }
                    }
                    else
                    {
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                        MessageBox.Show("من فضلك إضغط على زر تحديد الموقع");
                    }
                }
                else
                {
                    try
                    {
                        if (IsLocationConfirmed)
                        {
                            var db = new DataBaseContext(DataBaseContext.DBConnectionString);
                            try
                            {
                                var TestIncidents = db.AllIncidents.FirstOrDefault();
                            }
                            catch (Exception ex)
                            {
                                if (ex.Message.StartsWith("The specified table does not exist."))
                                {
                                    DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                                    databaseSchemaUpdater.AddTable<Incidents>();
                                    databaseSchemaUpdater.Execute();
                                }
                            }
                            var AddedIncident = db.AllIncidents.FirstOrDefault();
                            if (AddedIncident != null)
                            {
                                var r = db.AllIncidents.ToList();
                                db.AllIncidents.DeleteAllOnSubmit(r);
                                db.SubmitChanges();
                            }
                            Incidents AllAddedIncidents = new Incidents();
                            AllAddedIncidents.CallerNumber = long.Parse(mobNum.Text);
                            AllAddedIncidents.DeptID = DeptID;
                            AllAddedIncidents.DistrictID = DistrictID;
                            AllAddedIncidents.DistrictName = DistrictName;
                            AllAddedIncidents.IncidentDescription = req_description.Text;
                            AllAddedIncidents.Latitude = Coordinates[1];
                            AllAddedIncidents.LocationDescription = description.Text;
                            AllAddedIncidents.Longitude = Coordinates[0];
                            AllAddedIncidents.NationalID = long.Parse(identityNum.Text);
                            AllAddedIncidents.RoadID = RoadID;
                            AllAddedIncidents.RoadName = RoadName;
                            AllAddedIncidents.TypeID = IncidentCategoryType;
                            Incidents[] item = { AllAddedIncidents };
                            db.AllIncidents.InsertAllOnSubmit(item);
                            db.SubmitChanges();
                            MessageBox.Show("تم حفظ طلبك و سوف يتم ارسال الطلب تلقائياً بمجرد اتصالك بالانترنت");
                            NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                        }
                        else
                        {
                            MessageBox.Show("من فضلك إضغط على زر تحديد الموقع");
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("من فضلك تأكد من ادخال البيانات بشكل صحيح");
                    }
                
                   
            }
        }

       

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void lst_districts_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            if (lst_districts.SelectedItem != null)
            {
                districts d = lst_districts.SelectedItem as districts;
                if (d != null)
                {
                    DistrictName = d.LabelArStr;
                    DistrictID = d.ID;
                    int dID = d.ID;

                    roads = db.AllRoads.Where(r => r.ParentID == DistrictID).ToArray();
                    if (roads.Length == 0)
                    {
                        string[] items = new string[1];
                        items[0] = "لا توجد شوارع بهذا الحي";

                        lst_Streets.ItemsSource = items;

                        emptyStreets.Visibility = Visibility.Visible;
                        lst_Streets.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        lst_Streets.ItemsSource = roads;
                        lst_Streets.SelectedIndex = 0;
                        emptyStreets.Visibility = Visibility.Collapsed;
                        lst_Streets.Visibility = Visibility.Visible;
                    }
                }               
            }
        }

        bool Loaded = false;
        private void lst_Streets_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (lst_Streets.SelectedItem != null)
            {

                if (lst_Streets.SelectedItem.ToString() == "لا توجد شوارع بهذا الحي")
                {
                    RoadName = string.Empty;
                    RoadID = -1;
                }
                else
                {
                    allRoads selectedStreets = lst_Streets.SelectedItem as allRoads;
                    RoadName = selectedStreets.LabelArStr;
                    RoadID = selectedStreets.ID;
                }
            }
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (methods.user.RegistrationTypeId==3)
            {
                mobNum.Text = methods.businessUser.Mobile;
                mobNum.IsReadOnly = true;

                txtID.Text = "رقم السجل التجاري";
                identityNum.Text = methods.businessUser.IdentityBuisinessNumber.ToString();
                identityNum.IsReadOnly = true;
            }
            else
            {
                if (methods.user.RegistrationTypeId == 1)
                    txtID.Text = "رقم السجل المدني";
                else
                    txtID.Text = "رقم الإقامة";

                mobNum.Text = methods.individualUser.Mobile;
                mobNum.IsReadOnly = true;

                identityNum.Text = methods.individualUser.IdentityNumber.ToString();
                identityNum.IsReadOnly = true;
            }
            Loaded = true;
        }

        private void codeInConstructor()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            allDistricts = db.Districts.ToArray();
            lst_districts.ItemsSource = allDistricts;

            allIncidentCategories = db.IncidentCategories.ToArray();
            lst_incidentCategories.ItemsSource = allIncidentCategories;

            if (allIncidentCategories.Length != 0)
            {
                lst_incidentCategories.SelectedIndex = 0;
            }
        }

        private void lst_incidentCategories_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (lst_incidentCategories.SelectedItem != null)
            {
                incidentCategories I = lst_incidentCategories.SelectedItem as incidentCategories;
                if (I != null)
                {
                    DeptID = I.OId;
                    int ICID = I.OId;
                    incidentTypes = db.AllIncidentTypes.Where(t => t.CategoryID == ICID).ToArray();
                    if (incidentTypes.Length == 0)
                    {
                        string[] items = new string[1];
                        items[0] = "";
                        lst_incidentCategoriesTypes.ItemsSource = items;

                        emptyIncidentCatsTypes.Visibility = Visibility.Visible;
                        lst_incidentCategoriesTypes.Visibility = Visibility.Collapsed;

                    }
                    else
                    {
                        lst_incidentCategoriesTypes.ItemsSource = incidentTypes;
                        lst_incidentCategoriesTypes.SelectedIndex = 0;

                        emptyIncidentCatsTypes.Visibility = Visibility.Collapsed;
                        lst_incidentCategoriesTypes.Visibility = Visibility.Visible;
                    }
                }                
            }
        }

        private void lst_incidentCategoriesTypes_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (Loaded)
            {

                allIncidentTypes CategoryType = lst_incidentCategoriesTypes.SelectedItem as allIncidentTypes;
                if (CategoryType != null)
                {
                    IncidentCategoryType = CategoryType.OId;
                }
            }
        }

        private void btnSendMyLocation_Click_1(object sender, RoutedEventArgs e)
        {
            IsLocationConfirmed = true;
        }

        private async void mapWithMyLocation_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            Point p = e.GetPosition(mapWithMyLocation);
            MessageBox.Show("X = " + p.X + " Y = " + p.Y);
            // Get my current location.
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            GeoCoordinate myGeoCoordinate = CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);
            // Make my current location the center of the Map.
            this.mapWithMyLocation.Center = myGeoCoordinate;
            this.mapWithMyLocation.ZoomLevel = 13;
            // Create a small circle to mark the current location.
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Color.FromArgb(255, 130, 26, 24));
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;
            // Create a MapOverlay to contain the circle.
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;
            // Create a MapLayer to contain the MapOverlay.
            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);
            // Add the MapLayer to the Map.
            mapWithMyLocation.Layers.Add(myLocationLayer);

            
        }

         
    }
}