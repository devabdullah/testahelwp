﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class aboutNajranPage : PhoneApplicationPage
    {
        DataBaseContext db;
        methods methods = new methods();
        aboutNajran aboutNajran;
        aboutNajran[] aboutNaj = new aboutNajran[1];
        aboutNajran data;
        public aboutNajranPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            
        }
        BitmapImage bmi;
        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            loadPage();
                
        }

        void bmi_ImageOpened(object sender, RoutedEventArgs e)
        {
          //  MessageBox.Show("Opened");
            imgToBeDisplayed.Source = bmi;
        }
        private void loadPage()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            data = db.AboutNajran.FirstOrDefault();
            if (data != null)
            {
                if (data.image != null)
                {
                    bmi = methods.ByteArraytoBitmap(data.image);
                    bmi.CreateOptions = BitmapCreateOptions.BackgroundCreation;
                    imgToBeDisplayed.Source = bmi;
                }

                lblContent.Text = data.AboutApp;
            }
        }
        private void loadImage()
        {
            
        }
    }
}