﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;

namespace Testahel_WP_v1._0
{
    public partial class resetPasswordPage : PhoneApplicationPage
    {
        methods methods = new methods();
        public resetPasswordPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            userType.Items.Add("مواطن");
            userType.Items.Add("مقيم");
            userType.Items.Add("قطاع أعمال");
            userType.SelectedIndex = 0;
        }

        private void userType_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            
            if (userType.SelectedIndex==2)
            {
                tb_userType.Text = "رقم السجل التجاري";
            }
            else
            {
                if (userType.SelectedIndex==0)
                {
                    tb_userType.Text = "رقم السجل المدني ";
                }
                else
                {
                    tb_userType.Text = "رقم الإقامة";
                }
                
            }
        }


        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            if (txt_userName.Text == "")
            {
                MessageBox.Show("من فضلك تأكد من البيانات التى قمت بإدخالها");
            }
            else
            {
                string userTypeSelected = (userType.SelectedIndex + 1).ToString();
                string url = methods.serverURL + "ForgetPassword";
                string Data = "<GetPassword xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService\"><UserType>"+userTypeSelected+"</UserType><userName>"+txt_userName.Text+"</userName></GetPassword>";
                if (methods.checkNetworkConnection())
                {
                    string result = await methods.PostDataToServer(Data, url, "application/xml");
                    var r = JsonConvert.DeserializeObject<postResult>(result);
                    if (r.AffectedRows != 0)
                    {
                        MessageBox.Show(r.StatusMessage);
                        NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                    }
                    else
                        MessageBox.Show(r.StatusMessage);
                }
                else
                {
                    MessageBox.Show(methods.internetConnectionFailedMessage);
                }
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {
            
        }

        private void txt_userName_TextChanged_1(object sender, TextChangedEventArgs e)
        {
            
        }
    }
}