﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class mo3amlatyPage : PhoneApplicationPage
    {
        methods methods = new methods();
        myTreatments[] myTreatments;
        myTreatments[] treatments = new myTreatments[200];
        DataBaseContext db;
        public mo3amlatyPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }
        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (llsMyTreatments.ItemsSource == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                methods.setProgressIndicator(true);
                SystemTray.ProgressIndicator.Text = "يتم تحميل معاملاتي";
                SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);

                db = new DataBaseContext(DataBaseContext.DBConnectionString);
                myTreatments = await methods.getAllMyTreatments(methods.userID);

                if (myTreatments != null && myTreatments.Count() != 0)
                {
                    foreach (var treatment in myTreatments)
                    {
                        if (treatment.Subject.Length > 45)
                        {
                            treatment.displayedSubject = treatment.Subject.Substring(0, 45) + "...";
                        }
                        else
                            treatment.displayedSubject = treatment.Subject;
                    }
                    for (int i = 0; i < myTreatments.Length; i++)
                    {
                        string[] date = myTreatments[i].LetterDate.Split(' ');
                        myTreatments[i].LetterDate = date[0];
                        date = myTreatments[i].TransactionDate.Split(' ');
                        myTreatments[i].TransactionDate = date[0];
                    }
                }
                else
                {
                    myTreatments = db.MyTreatments.ToArray();
                    if (myTreatments.Count() == 0)
                    {
                        llsMyTreatments.Visibility = Visibility.Collapsed;
                        lblSpace.Visibility = Visibility.Visible;
                        lblMyTreatments.Visibility = Visibility.Visible;
                    }
                }

                if (myTreatments.Length > 200)
                {
                    for (int i = 0; i < treatments.Length; i++)
                    {
                        treatments[i] = new myTreatments();
                        treatments[i] = myTreatments[i];
                    }
                    llsMyTreatments.ItemsSource = treatments;
                }
                else
                    llsMyTreatments.ItemsSource = myTreatments;

                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
            }
        }
        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void llsMyTreatments_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            myTreatments myTreatment = llsMyTreatments.SelectedItem as myTreatments;
            if (myTreatment != null)
            {
                PhoneApplicationService.Current.State["myTreatment"] = myTreatment;
                NavigationService.Navigate(new Uri("/myTreatmentPage.xaml", UriKind.Relative));
            }            
        }

        
    }
}