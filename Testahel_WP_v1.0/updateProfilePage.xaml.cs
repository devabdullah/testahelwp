﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class updateProfilePage : PhoneApplicationPage
    {
        methods methods = new methods();
        DataBaseContext db;
        public updateProfilePage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            setData();
        }
        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
           // setData();
        }
        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            if (methods.checkNetworkConnection())
            {
                string Data = "";
                string Url = "";
                string contentType = "application/xml";

                if (methods.user.RegistrationTypeId == 3)
                {
                    banks bank = lstpBanks.SelectedItem as banks;
                    string bankID = bank.BankID.ToString();
                    //cities city = lstp_cities.SelectedItem as cities;
                    //string placeId = city.Id.ToString();
                    Url = methods.serverURL + "UpdateBuisinessSector";
                    Data = "<UpdateBuisinessSectorData xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Address>" + add.Text + "</Address><AttachmentImg></AttachmentImg><BankAccountNo>" + bankNum.Text + "</BankAccountNo><BankId>" + bankID + "</BankId><Email>" + email2.Text + "</Email><Fax>" + fax.Text + "</Fax><Mobile>" + mobNum2.Text + "</Mobile><Name>" + Name.Text + "</Name><POBox>" + poBox2.Text + "</POBox><Telephone>" + phoneNum2.Text + "</Telephone><UserToken>" + methods.user.UserToken + "</UserToken><ZipCode>" + code.Text + "</ZipCode></UpdateBuisinessSectorData>";
                }
                else
                {
                    maritalStatus ms = maritalStatus.SelectedItem as maritalStatus;
                    string maritalStatusId = ms.Id.ToString();
                    Url = methods.serverURL + "UpdateIndividualData";
                    Data = "<UpdateIndividualsData xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Email>" + email.Text + "</Email><FamilyName>" + lName.Text + "</FamilyName><FirstName>" + fName.Text + "</FirstName><MaritalStatusId>" + maritalStatusId + "</MaritalStatusId><Mobile>" + mobNum.Text + "</Mobile><POBox>" + poBox.Text + "</POBox><SecondName>" + sName.Text + "</SecondName><Telephone>" + phoneNum.Text + "</Telephone><ThirdName>" + thName.Text + "</ThirdName><UserToken>" + methods.user.UserToken + "</UserToken></UpdateIndividualsData>";
                }
                string result = await methods.PostDataToServer(Data, Url, contentType);
                var r = JsonConvert.DeserializeObject<postResult>(result);
                if (r.AffectedRows == 1)
                {
                    MessageBox.Show("تم التعديل بنجاح .");
                    var settings = IsolatedStorageSettings.ApplicationSettings;
                    if (settings.Contains("profileUpdated"))
                    {
                        settings["profileUpdated"] = "true";
                    }
                    else
                    {
                        settings.Add("profileUpdated", "true");
                    }
                    settings.Save();

                    if (NavigationService.CanGoBack)
                    {
                        NavigationService.GoBack();
                    }
                }
            }
            else
            {
                MessageBox.Show(methods.internetConnectionFailedMessage);
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private async void setData()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            if (methods.user.RegistrationTypeId == 3)
            {
                m6n.Visibility = Visibility.Collapsed;
                keta3A3mal.Visibility = Visibility.Visible;

                Name.Text = methods.businessUser.Name;
                add.Text = methods.businessUser.Address;
                mobNum2.Text = methods.businessUser.Mobile;
                phoneNum2.Text = methods.businessUser.Telephone;
                poBox2.Text = methods.businessUser.POBox;
                bankNum.Text = methods.businessUser.BankAccountNo;
                code.Text = methods.businessUser.ZipCode.ToString();
                email2.Text = methods.businessUser.Email;
                fax.Text = methods.businessUser.Fax;
                var allBanks = await methods.getAllBanks();
                lstpBanks.ItemsSource = allBanks;
                if (lstpBanks.ItemsSource != null)
                {
                    long id = methods.businessUser.BankId;
                    lstpBanks.SelectedItem = allBanks.Where(b => b.BankID == id).FirstOrDefault();

                }
                var allCities = await methods.getAllCities();
                //lstp_cities.ItemsSource = allCities;
                //if (lstp_cities.ItemsSource != null)
                //{
                //    long id = methods.businessUser.CommercialRegSourcePlaceId;
                     
                //    lstp_cities.SelectedItem = allCities.Where(c => c.Id == id).FirstOrDefault();
                //}
            }
            else
            {
                m6n.Visibility = Visibility.Visible;
                keta3A3mal.Visibility = Visibility.Collapsed;
                fName.Text = methods.individualUser.FirstName;
                sName.Text = methods.individualUser.SecondName;
                thName.Text = methods.individualUser.ThirdName;
                lName.Text = methods.individualUser.FamilyName;
                mobNum.Text = methods.individualUser.Mobile;
                phoneNum.Text = methods.individualUser.Telephone;
                email.Text = methods.individualUser.Email;
                poBox.Text = methods.individualUser.POBox;
                var allMaritalStatus = db.MaritalStatus.ToList();
                maritalStatus.ItemsSource = allMaritalStatus;
                if (maritalStatus.ItemsSource!=null)
                {
                    long id = methods.individualUser.MaritalStatusId;
                    maritalStatus.SelectedItem = allMaritalStatus.Where(m => m.Id == id).FirstOrDefault();
                }
            }
        }

        private void btnChangePassword_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/changePasswordPage.xaml",UriKind.Relative));
        }

        private void btnAttachement_Click_1(object sender, RoutedEventArgs e)
        {

        }
    }
}