﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Data.Linq;
using System.Windows.Media;


namespace Testahel_WP_v1._0
{
    public partial class evaluationPage : PhoneApplicationPage
    {
        methods methods = new methods();
        long NationalID;
        public evaluationPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            if (methods.checkNetworkConnection())
            {
                if (satisfied.IsChecked == false && unSatisfied.IsChecked == false)
                {
                    MessageBox.Show("من فضلك اكمل البيانات المدخلة");
                }
                else if (txtReasons.Text == "")
                {
                    MessageBox.Show("من فضلك اكمل البيانات المدخلة");
                    txtReasons.Focus();
                }
                else if (txtSuggestions.Text == "")
                {
                    MessageBox.Show("من فضلك أكمل البيانات المدخلة");
                    txtSuggestions.Focus();
                }
                else
                {
                    SystemTray.ProgressIndicator = new ProgressIndicator();
                    methods.setProgressIndicator(true);
                    SystemTray.ProgressIndicator.Text = "يتم إرسال التقييم";
                    SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    evaluations evaluation = new evaluations();
                    if (satisfied.IsChecked == true)
                    {
                        evaluation.IsSatisfied = true;
                    }
                    else
                    {
                        evaluation.IsSatisfied = false;
                    }
                    if (methods.user.RegistrationTypeId==3)
                    {
                        NationalID = methods.businessUser.IdentityBuisinessNumber;
                    }
                    else
                    {
                        NationalID = methods.individualUser.HefezaNumber;
                    }
                    evaluation.NationalID =NationalID;
                    evaluation.Reasons = txtReasons.Text;
                    evaluation.Suggestions = txtSuggestions.Text;

                    string xml = "<EvaluationArray xmlns=\"http://schemas.datacontract.org/2004/07/MobileService.Models\"><evaluations><Evaluation><IsSatisfied>" + evaluation.IsSatisfied.ToString().ToLower() + "</IsSatisfied><NationalID>" + evaluation.NationalID + "</NationalID><Reasons>" + evaluation.Reasons + "</Reasons><Suggestions>" + evaluation.Suggestions + "</Suggestions></Evaluation></evaluations></EvaluationArray>";
                    WebClient wc = new WebClient();
                    wc.Encoding = System.Text.Encoding.UTF8;
                    wc.Headers[HttpRequestHeader.ContentType] = "application/xml";
                    wc.UploadStringAsync(new Uri(methods.mobileService + "AddEvaluation", UriKind.RelativeOrAbsolute), "POST", xml);
                    wc.UploadStringCompleted += wc_UploadStringCompleted;
                }
            }
            else
            {
                var db = new DataBaseContext(DataBaseContext.DBConnectionString);
                try
                {
                    var eval = db.Evaluations.FirstOrDefault();
                    MessageBox.Show(eval.Suggestions);
                }
                catch (Exception ex)
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<evaluations>();
                        databaseSchemaUpdater.Execute();
                    }
                }
                var AddedEvaluations = db.Evaluations.FirstOrDefault();
                if (AddedEvaluations != null)
                {
                    var r = db.Evaluations.ToList();
                    db.Evaluations.DeleteAllOnSubmit(r);
                    db.SubmitChanges();
                }
                if (satisfied.IsChecked == false && unSatisfied.IsChecked == false)
                {
                    MessageBox.Show("من فضلك اكمل البيانات المدخلة");
                }
                else if (txtReasons.Text == "")
                {
                    MessageBox.Show("من فضلك اكمل البيانات المدخلة");
                    txtReasons.Focus();
                }
                else if (txtSuggestions.Text == "")
                {
                    MessageBox.Show("من فضلك أكمل البيانات المدخلة");
                    txtSuggestions.Focus();
                }
                else
                {
                    evaluations AllAddedEvaluations = new evaluations();
                    AllAddedEvaluations.IsSatisfied = (bool)satisfied.IsChecked;
                    AllAddedEvaluations.NationalID = NationalID;
                    AllAddedEvaluations.Reasons = txtReasons.Text;
                    AllAddedEvaluations.Suggestions = txtSuggestions.Text;
                    evaluations[] item = { AllAddedEvaluations };
                    db.Evaluations.InsertAllOnSubmit(item);
                    db.SubmitChanges();
                    MessageBox.Show("تم حفظ طلبك و سوف يتم ارسال الطلب تلقائياً بمجرد اتصالك بالانترنت");
                    NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                }
            }
        }

        void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            try
            {
                if (e.Result == "0")
                {
                    MessageBox.Show("حدث خطأ ما برجاء المحاولة مره أُخري");
                }
                else
                {
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    MessageBox.Show("تم تسجيل تقييمك بنجاح");
                    NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("لا يوجد اتصال بالشبكة");
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }
    }
}