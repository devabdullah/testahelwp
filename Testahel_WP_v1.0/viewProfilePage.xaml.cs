﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class viewProfilePage : PhoneApplicationPage
    {
        DataBaseContext db;
        methods methods = new methods();
        individualUserData individualUserData;
        businessSectorData businessUserData;
        public viewProfilePage()
        {
            InitializeComponent();

            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }
        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            if (methods.checkNetworkConnection())
            {
                var settings = IsolatedStorageSettings.ApplicationSettings;
                if (settings.Contains("profileUpdated"))
                {
                    if (settings["profileUpdated"] == "true")
                    {
                        string Data = "<UserToken xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService\"><userToken>" + methods.user.UserToken + "</userToken></UserToken>";
                        string url = "";
                        if (methods.user.RegistrationTypeId == 3)
                        {

                            //data.RegistrationType = "قطاع أعمال";
                            url = methods.serverURL + "GetBuisinessSectorData";
                            string result = await methods.PostDataToServer(Data, url, "application/xml");
                            businessUserData = JsonConvert.DeserializeObject<businessSectorData>(result);
                            var all = db.BusinessSectorData.ToList();
                            db.BusinessSectorData.DeleteAllOnSubmit(all);
                            db.SubmitChanges();
                            db.BusinessSectorData.InsertOnSubmit(businessUserData);
                            db.SubmitChanges();
                        }
                        else
                        {
                            if (methods.user.RegistrationTypeId == 1)
                            {
                                //data.RegistrationType = "مواطن";
                                url = methods.serverURL + "GetCitizenIndividualsData";

                            }
                            else
                            {
                                //data.RegistrationType = "مقيم";
                                url = methods.serverURL + "GetForeignIndividualsData";

                            }
                            string result = await methods.PostDataToServer(Data, url, "application/xml");
                            individualUserData = JsonConvert.DeserializeObject<individualUserData>(result);
                            var all = db.IndividualUserData.ToList();
                            db.IndividualUserData.DeleteAllOnSubmit(all);
                            db.SubmitChanges();
                            db.IndividualUserData.InsertOnSubmit(individualUserData);
                            db.SubmitChanges();
                        }
                        methods = new methods();
                        settings["profileUpdated"] = "false";
                    }
                }
            }
            setData();
        }
        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void btnEditProfile_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/updateProfilePage.xaml", UriKind.Relative));
        }

        private void setData()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            userType.Text = methods.user.RegistrationType;
            if (methods.user.RegistrationTypeId == 3)
            {

                m6n.Visibility = Visibility.Collapsed;
                keta3A3mal.Visibility = Visibility.Visible;

                identityNo.Text = methods.businessUser.IdentityBuisinessNumber.ToString();
                Name.Text = methods.businessUser.Name;
                long placeID = methods.businessUser.CommercialRegSourcePlaceId;
                try
                {
                    place.Text = db.Cities.Where(p => p.Id == placeID).FirstOrDefault().Title;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
                
                date.Text = methods.businessUser.CommercialRegStringDateHijri;
                add.Text = methods.businessUser.Address;
                phoneNum2.Text = methods.businessUser.Mobile;
                mobNum2.Text = methods.businessUser.Telephone;
                poBox.Text = methods.businessUser.POBox;
                rmz.Text = methods.businessUser.ZipCode.ToString();
                fax.Text = methods.businessUser.Fax;
                email2.Text = methods.businessUser.Email;
                long bankID = methods.businessUser.BankId;
                bank.Text = db.Banks.Where(b => b.BankID == bankID).FirstOrDefault().BankName;
                bankNum.Text = methods.businessUser.BankAccountNo;
            }
            else
            {
                if (methods.user.RegistrationTypeId == 1)
                {
                    txtUserName.Text = "رقم السجل المدني";
                    stkHafezaDate.Visibility = Visibility.Visible;
                    stkHafezaNum.Visibility = Visibility.Visible;
                    stkHafezaPlace.Visibility = Visibility.Visible;

                    no_7afeza.Text = methods.individualUser.HefezaNumber.ToString();
                    msdr_7afeza.Text = methods.individualUser.HefezaIssueSourcePlace;
                    txtHafezaDate.Text = methods.individualUser.HefezaIssueDateHijriString;
                }
                else
                {
                    txtUserName.Text = "رقم الإقامة";
                    stkHafezaPlace.Visibility = Visibility.Collapsed;
                    stkHafezaNum.Visibility = Visibility.Collapsed;
                    stkHafezaDate.Visibility = Visibility.Collapsed;
                }
                m6n.Visibility = Visibility.Visible;
                keta3A3mal.Visibility = Visibility.Collapsed;

                Num.Text = methods.individualUser.IdentityNumber.ToString();
                fName.Text = methods.individualUser.FirstName;
                sName.Text = methods.individualUser.SecondName;
                thName.Text = methods.individualUser.ThirdName;
                lName.Text = methods.individualUser.FamilyName;
                placeOfBirth.Text = methods.individualUser.BirthPlace;
                dateOfBirth.Text = methods.individualUser.BirthDateHijriString;


                address.Text = methods.individualUser.POBox;
                mobNum.Text = methods.individualUser.Mobile;
                phoneNum.Text = methods.individualUser.Telephone;
                email.Text = methods.individualUser.Email;
                int maritalStatusID = methods.individualUser.MaritalStatusId;
                var m = db.MaritalStatus.Where(ms => ms.Id == maritalStatusID).FirstOrDefault();
                if (m != null)
                {
                    maritalStatus.Text = m.Title;
                }
            }
        }


    }
}