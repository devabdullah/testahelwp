﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Testahel_WP_v1._0
{
    public partial class investmentAnnouncementDetailsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        public investmentAnnouncementDetailsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            investmentAnnouncements investmentAnnouncement = (investmentAnnouncements)PhoneApplicationService.Current.State["investmentAnnouncement"];
            investmentAnnouncements[] Details = new investmentAnnouncements[1];
            Details[0] = new investmentAnnouncements();
            Details[0] = investmentAnnouncement;
            lls_investmentAnnouncementDetails.ItemsSource = Details;
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }
    }
}