﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class pollsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        int FirstAnswerCount, SecondAnswerCount, ThirdAnswerCount, FourthAnswerCount;
        poll poll;
        DataBaseContext db;
        public pollsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }
        string[] answers;
        string FirstAnswer, SecondAnswer, ThirdAnswer, FourthAnswer;
        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            //poll = await methods.getPoll();
            if (poll == null)
            {
                if (methods.checkNetworkConnection())
                {
                    SystemTray.ProgressIndicator = new ProgressIndicator();
                    methods.setProgressIndicator(true);
                    SystemTray.ProgressIndicator.Text = "يتم تحميل السؤال";
                    SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    poll LastPoll = PhoneApplicationService.Current.State["CurrentPoll"] as poll;
                    poll = LastPoll;
                    //poll = await methods.getPoll();

                    //poll = db.Poll.FirstOrDefault();
                    
                    txtQuestion.Text = poll.Question;
                    answers = new string[poll.QuestionAnswers.Count()];
                    int i = 0;
                    foreach (pollAnswers answer in poll.QuestionAnswers)
                    {
                        answers[i] = answer.Answer;
                        i++;
                    }
                    opt1.Content = poll.QuestionAnswers[0].Answer;
                    opt2.Content = poll.QuestionAnswers[1].Answer;
                    opt3.Content = poll.QuestionAnswers[2].Answer;
                    opt4.Content = poll.QuestionAnswers[3].Answer;

                    sp_options.Visibility = Visibility.Visible;
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                }
                else
                {

                }
                //poll = db.Poll.FirstOrDefault();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            methods.setProgressIndicator(true);
            SystemTray.ProgressIndicator.Text = "يتم إرسال الإجابة";
            SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
            SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
            if (opt1.IsChecked == false && opt2.IsChecked == false && opt3.IsChecked == false && opt4.IsChecked == false)
            {
                MessageBox.Show("من فضلك اختر احد الاختيارات المعروضه امامك");
            }
            else
            {
                long ID;
                if (opt1.IsChecked == true)
                {
                    ID = poll.QuestionAnswers[0].ID;

                }
                else if (opt2.IsChecked == true)
                {
                    ID = poll.QuestionAnswers[1].ID;

                }
                else if (opt3.IsChecked == true)
                {
                    ID = poll.QuestionAnswers[2].ID;

                }
                else
                {
                    ID = poll.QuestionAnswers[3].ID;

                }
                FirstAnswerCount = poll.QuestionAnswers[0].Count;
                SecondAnswerCount = poll.QuestionAnswers[1].Count;
                ThirdAnswerCount = poll.QuestionAnswers[2].Count;
                FourthAnswerCount = poll.QuestionAnswers[3].Count;
                string xml = "<SubmitPollArray xmlns=\"http://schemas.datacontract.org/2004/07/MobileService.Models\"><SubmitPolls><SubmitPoll><AnswerID>" + ID + "</AnswerID><QuestionID>" + poll.ID + "</QuestionID></SubmitPoll></SubmitPolls></SubmitPollArray>";

                WebClient wc = new WebClient();
                wc.Encoding = System.Text.Encoding.UTF8;
                wc.Headers[HttpRequestHeader.ContentType] = "application/xml";

                wc.UploadStringAsync(new Uri(methods.mobileService+"SubmitPoll", UriKind.RelativeOrAbsolute), "POST", xml);
                wc.UploadStringCompleted += wc_UploadStringCompleted;
            }
        }

        void wc_UploadStringCompleted(object sender, UploadStringCompletedEventArgs e)
        {
            try
            {
                //string test = e.Result.ToString().Remove(0, 1);
                //string FinalResult = test.Remove(2,1);
                //MessageBox.Show(FinalResult);

                //string result = e.Result.ToString().Replace(@"""", "");
                //int Answer = int.Parse(result);
                if (e.Result.Contains("1"))
                {
                    pollTable ToBeSavedPoll = new pollTable();
                    ToBeSavedPoll.Question = poll.Question;
                    ToBeSavedPoll.ID = poll.ID;
                    db.PollTable.InsertOnSubmit(ToBeSavedPoll);
                    db.SubmitChanges();
                    for (int i = 0; i < 4; i++)
                    {
                        pollAnswersTable ToBeSavedAnswer = new pollAnswersTable();
                        ToBeSavedAnswer.Answer = poll.QuestionAnswers[i].Answer;
                        ToBeSavedAnswer.Count = poll.QuestionAnswers[i].Count;
                        ToBeSavedAnswer.pollID = poll.ID;
                        ToBeSavedAnswer.QuestionID = poll.QuestionAnswers[i].QuestionID;
                        db.PollAnswersTable.InsertOnSubmit(ToBeSavedAnswer);
                        db.SubmitChanges();
                    }

                    List<string> FullPollDetails = new List<string>();
                    FullPollDetails.Add(FirstAnswer);
                    FullPollDetails.Add(SecondAnswer);
                    FullPollDetails.Add(ThirdAnswer);
                    FullPollDetails.Add(FourthAnswer);

                    FullPollDetails.Add(FirstAnswerCount.ToString());
                    FullPollDetails.Add(SecondAnswerCount.ToString());
                    FullPollDetails.Add(ThirdAnswerCount.ToString());
                    FullPollDetails.Add(FourthAnswerCount.ToString());

                    FullPollDetails.Add(txtQuestion.Text);
                    PhoneApplicationService.Current.State["AnsweredPoll"] = poll;
                    var settings = IsolatedStorageSettings.ApplicationSettings;
                    settings["AnsweredPoll"] = poll;
                    settings.Save();
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    NavigationService.Navigate(new Uri("/PollsResult.xaml", UriKind.RelativeOrAbsolute));
                }
                else
                {
                    MessageBox.Show("حدث خطأ ما برجاء اعادة المحاولة مرة أخري");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}
