﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class typesofRequestsPage : PhoneApplicationPage
    {
        DataBaseContext db;
        requestTypes[] allRequestTypes;
        methods methods = new methods();
        public typesofRequestsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            if (lls_typesOfRequests.ItemsSource == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                methods.setProgressIndicator(true);
                SystemTray.ProgressIndicator.Text = "يتم تحميل اللوائح والإشتراطات";
                SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
            }
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            allRequestTypes = db.RequestTypes.ToArray();
            lls_typesOfRequests.ItemsSource = allRequestTypes;
            methods.setProgressIndicator(false);
            SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
            SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void lst_typesOfRequests_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            requestTypes r = lls_typesOfRequests.SelectedItem as requestTypes;
            if (r != null)
            {
                PhoneApplicationService.Current.State["Request"] = r;
                NavigationService.Navigate(new Uri("/ReqPage.xaml", UriKind.Relative));
            }
        }
    }
}