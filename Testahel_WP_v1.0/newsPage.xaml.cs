﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class newsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        DataBaseContext db;
        notifications[] allServerNotifications;
        notifications[] allNotifications = new notifications[10];
        public string imageSource;
        public newsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            var newsChecked = db.CheckedNotifications.Where(n => n.checkedItem == true && n.name.Contains("new")==true).FirstOrDefault();
            if (newsChecked != null)
            {
                if (methods.checkNetworkConnection())
                {
                    if (lls_news.ItemsSource == null)
                    {
                        SystemTray.ProgressIndicator = new ProgressIndicator();
                        methods.setProgressIndicator(true);
                        SystemTray.ProgressIndicator.Text = "يتم تحميل الأخبار";
                        SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    }
                    #region news
                    allServerNotifications = await methods.getNotifications();
                    if (allServerNotifications != null)
                    {
                        for (int i = 0; i < allServerNotifications.Length; i++)
                        {
                            allNotifications[i] = new notifications();
                            allNotifications[i] = allServerNotifications[i];
                            string[] date = allServerNotifications[i].Date.Split(' ');
                            allNotifications[i].Date = date[0];

                            if (allServerNotifications[i].Title.Length > 35)
                            {
                                allNotifications[i].displayedTitle = allServerNotifications[i].Title.Substring(0, 35) + "...";
                            }
                            else
                            {
                                allNotifications[i].displayedTitle = allServerNotifications[i].Title;
                            }
                            //_summary = allServerNotifications[i].Summary;
                            //_summary = _summary + "...";
                            //allNotifications[i].Summary = _summary;
                            //allNotifications[i].Details = allServerNotifications[i].Details;
                            imageSource = allServerNotifications[i].ImagePath;
                            imageSource = "http://www.najran.gov.sa" + imageSource;
                            allNotifications[i].ImagePath = imageSource;
                            //allNotifications[i].NewsURL = allServerNotifications[i].NewsURL;
                        }
                        lls_news.ItemsSource = allNotifications;
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    }
                    #endregion
                }
                else
                {
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    txtRemove_news.Visibility = Visibility.Collapsed;
                    txt_news.Visibility = Visibility.Visible;
                    txt_news.Text = methods.internetConnectionFailedMessage;
                    lls_news.Visibility = Visibility.Collapsed;
                }
            }

            else
            {
                txtRemove_news.Visibility = Visibility.Visible;
                txt_news.Visibility = Visibility.Collapsed;
                lls_news.Visibility = Visibility.Collapsed;
            }
            
        }

        private void lls_news_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            notifications mobileFeed = (notifications)lls_news.SelectedItem;
            if (mobileFeed != null)
            {
                PhoneApplicationService.Current.State["mobileFeed"] = mobileFeed;
                NavigationService.Navigate(new Uri("/newDetailsPage.xaml", UriKind.Relative));
            }
        }
    }
}