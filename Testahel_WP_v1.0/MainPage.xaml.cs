﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Testahel_WP_v1._0.Resources;
using Microsoft.Phone.Net.NetworkInformation;
using Testahel_WP_v1;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Globalization;
using System.Windows.Resources;
using org.jpedal.jbig2.io;
using System.IO;
using Microsoft.Phone.Data.Linq;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class MainPage : PhoneApplicationPage
    {
        //IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        DataBaseContext db;
        methods methods = new methods();
        individualUserData individualUserData;
        businessSectorData businessUserData;
        loginData data = new loginData();
        // Constructor
        public MainPage()
        {
            InitializeComponent();
            System.Threading.Thread.Sleep(3000);
        }
        
        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void btn_register_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/registerationPage.xaml", UriKind.Relative));
        }

        private async void btn_login_Click_1(object sender, RoutedEventArgs e)
        {
            bool validation = validate();
            if (validation)
            {
                if (methods.checkNetworkConnection())
                {
                    SystemTray.ProgressIndicator = new ProgressIndicator();
                    methods.setProgressIndicator(true);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ProgressIndicator.Text = "جار تسجيل الدخول";
                    //WebClient client = new WebClient();
                    //client.Headers[HttpRequestHeader.ContentType] = "application/xml";
                    string strData = "<LogIn xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Password>" + password.Password + "</Password><UserName>" + txtUserName.Text + "</UserName></LogIn>";
                    string strUrl = methods.serverURL + "Login";
                    string result = await methods.PostDataToServer(strData, strUrl, "application/xml");
                    if (result!=""&&!result.Contains("error"))
                    {
                        db = new DataBaseContext(DataBaseContext.DBConnectionString);
                            data = JsonConvert.DeserializeObject<loginData>(result);
                            if (data.IsLogin == true)
                            {
                                string Data = "<UserToken xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService\"><userToken>" + data.UserToken + "</userToken></UserToken>";
                                string url = "";
                                if (data.RegistrationTypeId == 1 || data.RegistrationTypeId == 2)
                                {
                                    if (data.RegistrationTypeId == 1)
                                    {
                                        data.RegistrationType = "مواطن";
                                        url = methods.serverURL + "GetCitizenIndividualsData";

                                    }
                                    else
                                    {
                                        data.RegistrationType = "مقيم";
                                        url = methods.serverURL + "GetForeignIndividualsData";

                                    }
                                    string result2 = await methods.PostDataToServer(Data, url, "application/xml");
                                    individualUserData = JsonConvert.DeserializeObject<individualUserData>(result2);
                                    db.IndividualUserData.InsertOnSubmit(individualUserData);
                                    db.SubmitChanges();
                                }
                                else
                                {
                                    data.RegistrationType = "قطاع أعمال";
                                    url = methods.serverURL + "GetBuisinessSectorData";
                                    string result2 = await methods.PostDataToServer(Data, url, "application/xml");
                                    businessUserData = JsonConvert.DeserializeObject<businessSectorData>(result2);
                                    db.BusinessSectorData.InsertOnSubmit(businessUserData);
                                    db.SubmitChanges();
                                }
                                loginData loginData = new loginData();
                                loginData = data;
                                db.LoginData.InsertOnSubmit(loginData);
                                db.SubmitChanges();

                                var settings = IsolatedStorageSettings.ApplicationSettings;
                                if (settings.Contains("isLogin"))
                                {
                                    settings["isLogin"] = "true";
                                }
                                else
                                {
                                    settings.Add("isLogin", "true");
                                }
                                settings.Save();

                                methods.getData();
                                methods.setProgressIndicator(false);
                                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                                NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.Relative));
                            }
                            else
                            {
                                methods.setProgressIndicator(false);
                                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                                MessageBox.Show("خطأ فى اسم المستخدم او كلمة المرور");
                            }
                    }
                    else
                    {
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                        MessageBox.Show(methods.errorMessage);
                    }
                    //client.UploadStringAsync(new Uri("http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/Login", UriKind.RelativeOrAbsolute), "POST", xml);
                    //client.UploadStringCompleted+=client_UploadStringCompleted;
                }
                else
                {
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    MessageBox.Show(methods.internetConnectionFailedMessage);
                }
            }
            else
            {
                MessageBox.Show("من فضلك تأكد من البيانات التي قمت بادخالها");
            }
        }

        private void btn_resetPassword_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/resetPasswordPage.xaml", UriKind.Relative));
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        public bool validate()
        {
            bool txtUserName_isEmpty = methods.isEmpty(txtUserName.Name, this);
            bool password_isEmpty = methods.isEmpty(password.Name, this);

            if (!txtUserName_isEmpty && !password_isEmpty)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void btn_showPass_MouseEnter_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            methods.showPassword(password, showPass);
        }

        private void btn_showPass_MouseLeave_1(object sender, System.Windows.Input.MouseEventArgs e)
        {
            methods.hidePass(password, showPass);
        }

        private void password_PasswordChanged_1(object sender, RoutedEventArgs e)
        {
            if (password.Password == "")
            {
                methods.hideEye(btn_showPass);
            }
            else
            {
                methods.showEye(btn_showPass);
            }
        }

        private void showPass_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Terminate();
        }
    }
}