﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;

namespace Testahel_WP_v1._0
{
    public partial class ShowRegulationData : PhoneApplicationPage
    {
        methods methods = new methods();
        RegulationsData item;
        public ShowRegulationData()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            this.Loaded += ShowRegulationData_Loaded;
        }

        void ShowRegulationData_Loaded(object sender, RoutedEventArgs e)
        {
            item = PhoneApplicationService.Current.State["RegulationData"] as RegulationsData;
            if (item != null)
            {
                lblName.Text = item.Name;
                lblDescription.Text = item.Description;
                //hyperlinkDownload.Content = item.Link;
                //hyperlinkDownload.NavigateUri = new Uri(item.Link, UriKind.RelativeOrAbsolute);
            }
            
        }

        private async void btn_download_Click_1(object sender, RoutedEventArgs e)
        {
            if (item.Link.Contains("."))
            {
                await Launcher.LaunchUriAsync(new Uri(item.Link));
            }
            
        }
    }
}