﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace Testahel_WP_v1._0
{
    public partial class tenderAnnouncementDetailsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        public tenderAnnouncementDetailsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            tenderAnnouncements tenderAnnouncements = (tenderAnnouncements)PhoneApplicationService.Current.State["tenderAnnouncements"];
            tenderAnnouncements[] Details = new tenderAnnouncements[1];
            Details[0] = new tenderAnnouncements();
            Details[0] = tenderAnnouncements;
            lls_tenderAnnouncement.ItemsSource = Details;
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {
            
        }
    }
}