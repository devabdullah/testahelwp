﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class personalSettingsPage : PhoneApplicationPage
    {
        DataBaseContext db;
        methods methods = new methods();
        IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
        public personalSettingsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }
        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);            
            var checkedNot = db.CheckedNotifications.Where(ch => ch.checkedItem == true).ToList();
            foreach (var item in checkedNot)
            {
                CheckBox b = this.FindName(item.name) as CheckBox;
                b.IsChecked = true;
            }
            settings["checkedItems"] = checkedNot;
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            var newsItem = db.CheckedNotifications.Where(n => n.name == news.Name).FirstOrDefault();
            newsItem.checkedItem = (bool)news.IsChecked;
            var announcementsItem = db.CheckedNotifications.Where(ann => ann.name == announcements.Name).FirstOrDefault();
            announcementsItem.checkedItem = (bool)announcements.IsChecked;
            var chancesItem = db.CheckedNotifications.Where(ch => ch.name == chances.Name).FirstOrDefault();
            chancesItem.checkedItem = (bool)chances.IsChecked;
            db.SubmitChanges();
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }


    }
}