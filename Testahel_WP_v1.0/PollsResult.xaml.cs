﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class PollsResult : PhoneApplicationPage
    {
        methods methods = new methods();
        public PollsResult()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            this.Loaded += PollsResult_Loaded;
        }
        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
        }
        void PollsResult_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //poll FullPoll = PhoneApplicationService.Current.State["AnsweredPoll"] as poll;
                var settings = IsolatedStorageSettings.ApplicationSettings;
                poll FullPoll = settings["CurrentPoll"] as poll;
                ContentPanel.Visibility = Visibility.Visible;
                txtFirst.Text = FullPoll.QuestionAnswers[0].Answer;
                txtSecond.Text = FullPoll.QuestionAnswers[1].Answer;
                txtThird.Text = FullPoll.QuestionAnswers[2].Answer;
                txtFourth.Text = FullPoll.QuestionAnswers[3].Answer;
                txtQuestion.Text = FullPoll.Question;
                double FirstCount = int.Parse(FullPoll.QuestionAnswers[0].Count.ToString()), SecondCount = int.Parse(FullPoll.QuestionAnswers[1].Count.ToString()), ThirdCount = int.Parse(FullPoll.QuestionAnswers[2].Count.ToString()), FourthCount = int.Parse(FullPoll.QuestionAnswers[3].Count.ToString());
                double FullMark = FirstCount + SecondCount + ThirdCount + FourthCount;

                double FirstPercent = (FirstCount / FullMark) * 100;
                progfirs.Width = (int)FirstPercent * 5;
                txtfirstper.Text = ((int)FirstPercent).ToString() + "%";

                double SecondPercent = (SecondCount / FullMark) * 100;
                progSecond.Width = (int)SecondPercent * 5;
                txtSecondper.Text = ((int)SecondPercent).ToString() + "%";

                double ThirdPercent = (ThirdCount / FullMark) * 100;
                progThird.Width = (int)ThirdPercent * 5;
                txtThirdper.Text = ((int)ThirdPercent).ToString() + "%";

                double FourthPercent = (FourthCount / FullMark) * 100;
                progFourth.Width = FourthPercent * 5;
                txtFourthper.Text = ((int)FourthPercent).ToString() + "%";
            }
            catch (Exception ex)
            {
                lblError.Text = methods.errorMessage;
                lblError.Visibility = Visibility.Visible;
            }
        }
    }
}