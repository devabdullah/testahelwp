﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using System.Globalization;
using System.Windows.Threading;
using System.Diagnostics;
using Newtonsoft.Json;
using Microsoft.Phone.Net.NetworkInformation;
using System.Windows.Media.Imaging;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Data.Linq;
using System.Threading;

namespace Testahel_WP_v1._0
{
    public partial class methods : PhoneApplicationPage
    {
        #region Global Variables
        DataBaseContext db;
        requestTypes[] rTypes;
        myRequests[] myRequests;
        myTreatments[] myTreatments;
        emergencyGuid[] emergrncyGuideContacts;
        aboutNajran aboutNajran;
        districts[] districts;
        allRoads[] allRoads;
        allIncidentTypes[] allIncidentTypes;
        incidentCategories[] incidentCats;
        maritalStatus[] allMaritalStatus;
        banks[] allBanks;
        cities[] allCities;
        public loginData user;
        public individualUserData individualUser;
        public businessSectorData businessUser;
        public loginData loginData;
        public string internetConnectionFailedMessage = "عفوا ! يوجد خطأ فى الاتصال بالشبكة";
        public string errorMessage = "عفوا ! حدث خطأ ما برجاء التأكد من البيانات ثم حاول مرة أخري";
        public string userName = "";
       // private string serverUrl = " http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc";
        private string gateUrl = "http://www.najran.gov.sa/Service";
        public string mobileService = "http://46.240.65.62:6644/MobileService.svc/";
        public string serverURL = "http://46.240.65.62:6633/Service.svc/";
        private int noofNotifications = 10;
        public long userID;
        #endregion

        public methods()
        {
            InitializeComponent();
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            
            #region add loginData
            try
            {
                var d = db.LoginData.FirstOrDefault();
            }
            catch (Exception ex)
            {

                try
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<loginData>();
                        databaseSchemaUpdater.Execute();
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                catch (Exception exc)
                {

                    MessageBox.Show(exc.Message);
                }
            }
            #endregion

            #region add individualUserData
            try
            {
                var d = db.IndividualUserData.FirstOrDefault();
            }
            catch (Exception ex)
            {

                try
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<individualUserData>();
                        databaseSchemaUpdater.Execute();
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                catch (Exception exc)
                {

                    MessageBox.Show(exc.Message);
                }
            }
            #endregion

            #region add businessUserData
            try
            {
                var d = db.BusinessSectorData.FirstOrDefault();
            }
            catch (Exception ex)
            {

                try
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<businessSectorData>();
                        databaseSchemaUpdater.Execute();
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                catch (Exception exc)
                {

                    MessageBox.Show(exc.Message);
                }
            }
            #endregion

            loginData = db.LoginData.OrderByDescending(i => i.idPK).FirstOrDefault();
            if (loginData != null)
            {
                if (loginData.RegistrationTypeId == 1 || loginData.RegistrationTypeId == 2)
                {
                    individualUser = db.IndividualUserData.OrderByDescending(i => i.idPK).FirstOrDefault();
                    if (individualUser != null)
                    {
                        userName = individualUser.FirstName;
                        userID = individualUser.IdentityNumber;
                    }

                }
                else
                {
                    businessUser = db.BusinessSectorData.OrderByDescending(i => i.idPK).FirstOrDefault();
                    if (businessUser != null)
                    {
                        userName = businessUser.Name;
                        userID = businessUser.IdentityBuisinessNumber;
                    }
                    

                }
                user = loginData;
            }
            
            
        }

        public static bool checkNetworkConnection()
        {
            var ni = NetworkInterface.NetworkInterfaceType;

            bool IsConnected = false;
            if ((ni == NetworkInterfaceType.Wireless80211) || (ni == NetworkInterfaceType.MobileBroadbandCdma) || (ni == NetworkInterfaceType.MobileBroadbandGsm))
                IsConnected = true;
            else if (ni == NetworkInterfaceType.None)
                IsConnected = false;
            return IsConnected;
        }

        public bool isEmpty(string textbox_name, PhoneApplicationPage page)
        {
            TextBox t = page.FindName(textbox_name) as TextBox;

            try
            {
                if (t.Text != "")
                {
                    return false;
                }
                else
                {
                    return true;
                }

            }
            catch (Exception)
            {

                PasswordBox p = page.FindName(textbox_name) as PasswordBox;
                if (p.Password != "")
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }


        }

        #region Base64 Image Encoding
        public string GetBase64Encoding(BitmapImage imageUrl)
        {
            byte[] bytearray = null;
            using (MemoryStream ms = new MemoryStream())
            {
                if (imageUrl == null)
                {

                }
                else
                {
                    try
                    {
                        WriteableBitmap wbitmp = new WriteableBitmap(imageUrl);

                        wbitmp.SaveJpeg(ms, imageUrl.PixelWidth, imageUrl.PixelHeight, 0, 100);

                        bytearray = ms.ToArray();
                    }
                    catch (Exception ex)
                    {

                        MessageBox.Show(ex.Message);
                    }

                }
            }
            string str = Convert.ToBase64String(bytearray);

            return str;
        }
        #endregion

        public async Task<double[]> GetLongitudeLatitude()
        {
            Geolocator gl = new Geolocator();
            gl.DesiredAccuracyInMeters = 50;
            Geoposition geoposition = await gl.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
            double Longitude = geoposition.Coordinate.Longitude;
            double Latitude = geoposition.Coordinate.Latitude;
            double[] GeoCoordinates = { Longitude, Latitude };
            return GeoCoordinates;
        }

        public static BitmapImage ByteArraytoBitmap(byte[] byteArray)
        {
            MemoryStream stream = new MemoryStream(byteArray);
            BitmapImage bitmapImage = new BitmapImage();
            bitmapImage.SetSource(stream);
            return bitmapImage;
        }

        public byte[] ImageToArray(BitmapImage ChosenImageToBeSent)
        {
            BitmapImage image = ChosenImageToBeSent;
            image.CreateOptions = BitmapCreateOptions.None;

            WriteableBitmap wbmp = new WriteableBitmap(image);
            MemoryStream ms = new MemoryStream();
            wbmp.SaveJpeg(ms, wbmp.PixelWidth, wbmp.PixelHeight, 0, 100);
            return ms.ToArray();
        }
        public MemoryStream ImageToMemoryStream(BitmapImage ChosenImageToBeSent)
        {
            BitmapImage image = ChosenImageToBeSent;
            image.CreateOptions = BitmapCreateOptions.None;

            WriteableBitmap wbmp = new WriteableBitmap(image);
            MemoryStream ms = new MemoryStream();
            wbmp.SaveJpeg(ms, wbmp.PixelWidth, wbmp.PixelHeight, 0, 100);
            return ms;
        }

        public static byte[] ImageToBytes(BitmapImage img)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                WriteableBitmap btmMap = new WriteableBitmap(img);
                System.Windows.Media.Imaging.Extensions.SaveJpeg(btmMap, ms, img.PixelWidth, img.PixelHeight, 0, 100);
                img = null;
                return ms.ToArray();
            }
        }

        public async Task<string> PostData(string UrlString, string ContentType, string DataToBePosted)
        {
            #region Magic Request Works if xml, exception if json WTF!
            try
            {
                string Response = "";
                WebClient ClientToPostData = new WebClient();
                ClientToPostData.Encoding = Encoding.UTF8;
                ClientToPostData.Headers["Content-Type"] = ContentType;
                ClientToPostData.UploadStringAsync(new Uri(UrlString, UriKind.RelativeOrAbsolute), "POST", DataToBePosted);
                ClientToPostData.UploadStringCompleted += (s, ev) => { Response = ev.Result.ToString(); };
                return Response;
            }
            catch (Exception ex)
            {
                return "Exception : " + Environment.NewLine + ex.ToString();
            }
            #endregion
        }

        public Task<string> GetWebResultAsync(string url, string DataToSend)
        {
            var tcs = new TaskCompletionSource<string>();
            var client = new WebClient();
            client.Headers["Content-Type"] = "application/xml";
            UploadStringCompletedEventHandler h = null;
            h = (sender, args) =>
                    {
                        if (args.Cancelled)
                        {
                            tcs.SetCanceled();
                        }
                        else if (args.Error != null)
                        {
                            tcs.SetException(args.Error);
                        }
                        else
                        {
                            tcs.SetResult(args.Result);
                        }

                        client.UploadStringCompleted -= h;
                    };

            client.UploadStringCompleted += h;
            client.UploadStringAsync(new Uri(url, UriKind.RelativeOrAbsolute), "POST", DataToSend);
            return tcs.Task;
        }

        public async Task<districts[]> getAllDistricts()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllDistricts";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(districts[]));
                return (serializer.ReadObject(stream) as districts[]);
            }
        }

        public async Task<RegulationsData[]> GetAllRegulations()
        {
            HttpClient client = new HttpClient();
            string url = mobileService + "GetAllRegulations";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(RegulationsData[]));
                return (serializer.ReadObject(stream) as RegulationsData[]);
            }
        }

        public async Task<allRoads[]> getAllRoads()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllRoads";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(allRoads[]));
                return (serializer.ReadObject(stream) as allRoads[]);
            }
        }

        public async Task<incidentCategories[]> GetIncidentCategories()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetIncidentCategories";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(incidentCategories[]));
                return (serializer.ReadObject(stream) as incidentCategories[]);
            }
        }

        public async Task<allIncidentTypes[]> getAllIncidentTypes()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllIncidentTypes";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(allIncidentTypes[]));
                return (serializer.ReadObject(stream) as allIncidentTypes[]);
            }
        }

        public async Task<streets[]> getAllStreetsByDistrict(int districtID)
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetRoadsByDistricts?districtID=" + districtID;
            HttpResponseMessage response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(streets[]));
                return (serializer.ReadObject(stream) as streets[]);
            }
        }

        public async Task<aboutNajran> getAboutNajran()
        {
            HttpClient client = new HttpClient();
            string url = mobileService + "GetAbout";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(aboutNajran));
                return (serializer.ReadObject(stream) as aboutNajran);
            }
        }

        public async Task<poll> getPoll()
        {
            HttpClient client = new HttpClient();
            string url = mobileService + "GetPoll";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(poll));
                return (serializer.ReadObject(stream) as poll);
            }
        }

        public async Task<myRequests[]> getAllMyRequests(long nationalID)
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetERequestsByNationalID?NationalID=" + nationalID;
            var response = new HttpResponseMessage();
            response.Dispose();
           // response.Headers.Clear();
               response= await client.GetAsync(url);
            //response.Headers.Add("Server", response.Headers.Server.ToString());
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(myRequests[]));
                return (serializer.ReadObject(stream) as myRequests[]);
            }
        }

        public async Task<myTreatments[]> getAllMyTreatments(long nationalID)
        {
            HttpClient client = new HttpClient();
            string url = serverURL+"GetTransactionsByInvestorlID?InvestorID=" + nationalID;
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(myTreatments[]));
                return (serializer.ReadObject(stream) as myTreatments[]);
            }
        }

        public async Task<incidentCategoriesTypes[]> getAllIncidentCategoriesTypesByIC(int ICID)
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetIncidentTypesByCategoryID?catID=" + ICID;
            HttpResponseMessage response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(incidentCategoriesTypes[]));
                return (serializer.ReadObject(stream) as incidentCategoriesTypes[]);
            }
        }

        public async Task<notifications[]> getNotifications()
        {
            HttpClient client = new HttpClient();
            string url = gateUrl + "/MobileFeeds.ashx?topn=" + noofNotifications;
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(notifications[]));
                return (serializer.ReadObject(stream) as notifications[]);
            }
        }

        public async Task<tenderAnnouncements[]> getTenderAnnouncements()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetTenderAnnouncements";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(tenderAnnouncements[]));
                return (serializer.ReadObject(stream) as tenderAnnouncements[]);
            }
        }

        public async Task<investmentAnnouncements[]> getInvestmentAnnouncements()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetInvestmentAnnouncements";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(investmentAnnouncements[]));
                return (serializer.ReadObject(stream) as investmentAnnouncements[]);
            }
        }

        public async Task<emergencyGuid[]> getAllEmergencyGuid()
        {
            HttpClient client = new HttpClient();
            string url = mobileService + "GetAllGuides";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(emergencyGuid[]));
                return (serializer.ReadObject(stream) as emergencyGuid[]);
            }
        }

        public async Task<maritalStatus[]> getAllMaritalStatus()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllMaritalStatus";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(maritalStatus[]));
                return (serializer.ReadObject(stream) as maritalStatus[]);
            }
        }

        public async Task<banks[]> getAllBanks()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllBanks";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(banks[]));
                return (serializer.ReadObject(stream) as banks[]);
            }
        }

        public async Task<cities[]> getAllCities()
        {
            HttpClient client = new HttpClient();
            string url = serverURL + "GetAllCities";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(cities[]));
                return (serializer.ReadObject(stream) as cities[]);
            }
        }

        public async Task<requestTypes[]> getRequestTypes()
        {
            HttpClient client = new HttpClient();
            string url = serverURL+"GetERequestTypes";
            HttpResponseMessage response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(requestTypes[]));
                return (serializer.ReadObject(stream) as requestTypes[]);
            }
        }

        public async Task<individualUserData> getIndividualsData(string userToken, string url)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("UserToken", userToken);
            //string url = "http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/GetIndividualsData";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            MessageBox.Show(result);
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(individualUserData));
                return (serializer.ReadObject(stream) as individualUserData);
            }
        }

        public async Task<businessSectorData> getBuisinessSectorData(string userToken)
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("UserToken", userToken);
            string url = serverURL + "GetBuisinessSectorData";
            var response = await client.GetAsync(url);
            string result = await response.Content.ReadAsStringAsync();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(result)))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(businessSectorData));
                return (serializer.ReadObject(stream) as businessSectorData);
            }
        }

        public void showPassword(PasswordBox passwordbox ,TextBox textbox)
        {
            passwordbox.Visibility = Visibility.Collapsed;
            textbox.Visibility = Visibility.Visible;
            textbox.Text = passwordbox.Password;
        }


        public void hidePass(PasswordBox passwordbox, TextBox textbox)
        {
            passwordbox.Visibility = Visibility.Visible;
            textbox.Visibility = Visibility.Collapsed;
        }

        public void showEye(Button button)
        {
            button.Visibility = Visibility.Visible;
        }

        public void hideEye(Button button)
        {
            button.Visibility = Visibility.Collapsed;
        }
        public static void setProgressIndicator(bool isVisible)
        {
            if (isVisible==false)
            {
                if (SystemTray.ProgressIndicator != null)
                {
                    SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
                    SystemTray.ProgressIndicator.IsVisible = isVisible;
                }
            }
            else
            {
                SystemTray.ProgressIndicator.IsIndeterminate = isVisible;
                SystemTray.ProgressIndicator.IsVisible = isVisible;
            }
            
        }

        #region
        Dictionary<int, string> monthNamesAr = new Dictionary<int, string>()
        {
            {01, "محرم"},
            {02, "صفر"},
            {03, "ربيع الاول"},
            {04, "ربيع الثانى"},
            {05, "جمادى الاول"},
            {06, "جمادى الاخر"},
            {07, "رجب"},
            {08, "شعبان"},
            {09, "رمضان"},
            {10, "شوال"},
            {11, "ذو القعدة"},
            {12, "ذو الحجة"}
        };
        #endregion

        public void setDateValue(TextBlock txtDateName)
        {
            txtDateName.Text = DateTime.Now.Date.ToString("dd" + " من " +monthNamesAr.Where(m => m.Key == int.Parse(DateTime.Now.Date.ToString("MM"))).FirstOrDefault().Value+ " " + "yyyy");
        }
        public async Task<string> PostDataToServer(string Data, string Url, string ContentType)
        {
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage msg = new HttpRequestMessage(new HttpMethod("POST"), new Uri(Url));
            msg.Content = new StringContent(Data);
            msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(ContentType);
            var response = await httpClient.SendAsync(msg);
            string res = await response.Content.ReadAsStringAsync();
            return res;
        }
        public async Task<string> getDataFromServer(string Url)
        {
            HttpClient httpClient = new HttpClient();
            HttpRequestMessage msg = new HttpRequestMessage(new HttpMethod("Get"), new Uri(Url));
            var response = await httpClient.SendAsync(msg);
            string res = await response.Content.ReadAsStringAsync();
            return res;
        }

        public async void getData()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            
            #region Settings
            try
            {
                var q1 = db.CheckedNotifications.FirstOrDefault();
            }
            catch (Exception ex)
            {
                try
                {
                    if (ex.Message.StartsWith("The specified table does not exist."))
                    {
                        DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                        databaseSchemaUpdater.AddTable<checkedNotifications>();
                        databaseSchemaUpdater.Execute();
                    }
                    else
                        MessageBox.Show(ex.Message);
                }
                catch (Exception exc)
                {

                    MessageBox.Show(exc.Message);
                }
            }
            var q = db.CheckedNotifications.FirstOrDefault();
            if (q == null)
            {
                checkedNotifications checkedNotifications = new checkedNotifications();
                checkedNotifications.name = "news";
                checkedNotifications.checkedItem = true;
                db.CheckedNotifications.InsertOnSubmit(checkedNotifications);
                db.SubmitChanges();
                checkedNotifications checkedNotifications2 = new checkedNotifications();
                checkedNotifications2.name = "announcements";
                checkedNotifications2.checkedItem = true;
                db.CheckedNotifications.InsertOnSubmit(checkedNotifications2);
                db.SubmitChanges();
                checkedNotifications checkedNotifications3 = new checkedNotifications();
                checkedNotifications3.name = "chances";
                checkedNotifications3.checkedItem = true;
                db.CheckedNotifications.InsertOnSubmit(checkedNotifications3);
                db.SubmitChanges();
            }
            #endregion

            if (methods.checkNetworkConnection())
            {


                #region Marital Status
                try
                {
                    var mS = db.MaritalStatus.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<maritalStatus>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var maritalS = db.MaritalStatus.FirstOrDefault();
                allMaritalStatus = await getAllMaritalStatus();
                if (allMaritalStatus != null)
                {
                    if (maritalS != null)
                    {

                        var m = db.MaritalStatus.ToList();
                        db.MaritalStatus.DeleteAllOnSubmit(m);
                        db.SubmitChanges();
                    }
                    db.MaritalStatus.InsertAllOnSubmit(allMaritalStatus);
                    db.SubmitChanges();
                }
                #endregion

                #region About Najran
                try
                {
                    var about = db.AboutNajran.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<aboutNajran>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var aboutN = db.AboutNajran.FirstOrDefault();
                aboutNajran = await getAboutNajran();
                
                if (aboutNajran != null)
                {
                    if (aboutN != null)
                    {
                        var aboutApp = db.AboutNajran.ToList();
                        db.AboutNajran.DeleteAllOnSubmit(aboutApp);
                        db.SubmitChanges();
                    }
                    BitmapImage bmi = new BitmapImage(new Uri(aboutNajran.ImageUrl, UriKind.RelativeOrAbsolute));
                    bmi.CreateOptions = BitmapCreateOptions.None;
                    bmi.ImageOpened += (s, a) =>
                    {
                        byte[] testArr = ImageToArray(bmi);
                        aboutNajran.image = testArr;
                    };

                }

                #endregion

                #region Banks
                try
                {
                    var bank = db.Banks.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<banks>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var b = db.Banks.FirstOrDefault();
                allBanks = await getAllBanks();
                if (allBanks != null)
                {
                    if (b != null)
                    {
                        var bk = db.Banks.ToList();
                        db.Banks.DeleteAllOnSubmit(bk);
                        db.SubmitChanges();
                    }

                    db.Banks.InsertAllOnSubmit(allBanks);
                    db.SubmitChanges();
                }

                #endregion

                #region Cities
                try
                {
                    var c = db.Cities.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<cities>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var city = db.Cities.FirstOrDefault();
                allCities = await getAllCities();
                if (allCities != null)
                {
                    if (city != null)
                    {
                        var cities = db.Cities.ToList();
                        db.Cities.DeleteAllOnSubmit(cities);
                        db.SubmitChanges();
                    }

                    db.Cities.InsertAllOnSubmit(allCities);
                    db.SubmitChanges();
                }
                #endregion

                #region emergencyGuide
                try
                {
                    var emG1 = db.EmergencyGuid.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<emergencyGuid>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var emG = db.EmergencyGuid.FirstOrDefault();
                emergrncyGuideContacts = await getAllEmergencyGuid();
                if (emergrncyGuideContacts != null)
                {
                    if (emG != null)
                    {
                        var em = db.EmergencyGuid.ToList();
                        db.EmergencyGuid.DeleteAllOnSubmit(em);
                        db.SubmitChanges();
                    }

                    db.EmergencyGuid.InsertAllOnSubmit(emergrncyGuideContacts);
                    db.SubmitChanges();
                }
                #endregion

                if (aboutNajran != null)
                {
                    db.AboutNajran.InsertOnSubmit(aboutNajran);
                    db.SubmitChanges();
                }


                #region districts
                try
                {
                    var dis1 = db.Districts.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<districts>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var dis = db.Districts.FirstOrDefault();
                districts = await getAllDistricts();
                if (districts != null)
                {
                    if (dis != null)
                    {
                        var d = db.Districts.ToList();
                        db.Districts.DeleteAllOnSubmit(d);
                        db.SubmitChanges();
                    }

                    db.Districts.InsertAllOnSubmit(districts);
                    db.SubmitChanges();
                }

                #endregion

                #region roads
                try
                {
                    var road1 = db.AllRoads.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<allRoads>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var road = db.AllRoads.FirstOrDefault();
                allRoads = await getAllRoads();
                if (allRoads != null)
                {
                    if (road != null)
                    {
                        var r = db.AllRoads.ToList();
                        db.AllRoads.DeleteAllOnSubmit(r);
                        db.SubmitChanges();
                    }

                    db.AllRoads.InsertAllOnSubmit(allRoads);
                    db.SubmitChanges();
                }
                #endregion

                #region incident categories
                try
                {
                    var inc = db.IncidentCategories.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<incidentCategories>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var cat = db.IncidentCategories.FirstOrDefault();
                incidentCats = await GetIncidentCategories();
                if (incidentCats != null)
                {
                    if (cat != null)
                    {
                        var ic = db.IncidentCategories.ToList();
                        db.IncidentCategories.DeleteAllOnSubmit(ic);
                        db.SubmitChanges();
                    }

                    db.IncidentCategories.InsertAllOnSubmit(incidentCats);
                    db.SubmitChanges();
                }

                #endregion

                #region incident types
                try
                {
                    var itypes = db.AllIncidentTypes.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<allIncidentTypes>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var type = db.AllIncidentTypes.FirstOrDefault();
                allIncidentTypes = await getAllIncidentTypes();
                if (allIncidentTypes != null)
                {
                    if (type != null)
                    {
                        var incTypes = db.AllIncidentTypes.ToList();
                        db.AllIncidentTypes.DeleteAllOnSubmit(incTypes);
                        db.SubmitChanges();
                    }

                    db.AllIncidentTypes.InsertAllOnSubmit(allIncidentTypes);
                    db.SubmitChanges();
                }
                #endregion

                #region requestsTypes
                try
                {
                    var requests1 = db.RequestTypes.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<requestTypes>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var requests = db.RequestTypes.FirstOrDefault();
                rTypes = await getRequestTypes();
                if (rTypes != null)
                {
                    if (requests != null)
                    {
                        var r = db.RequestTypes.ToList();
                        db.RequestTypes.DeleteAllOnSubmit(r);
                        db.SubmitChanges();
                    }
                    db.RequestTypes.InsertAllOnSubmit(rTypes);
                    db.SubmitChanges();
                }



                #endregion

                #region myRequests
                try
                {
                    var myReqs1 = db.MyRequests.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<myRequests>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                var myReqs = db.MyRequests.FirstOrDefault();
                myRequests = await getAllMyRequests(userID);
                if (myRequests != null)
                {
                    foreach (var request in myRequests)
                    {
                        if (request.RequestStatus == "1")
                        {
                            request.RequestStatus = "مقبول";
                        }
                        else if (request.RequestStatus == "0")
                        {
                            request.RequestStatus = "مرفوض";
                        }
                        else
                            request.RequestStatus = "لم يتم النظر فيه";
                    }
                    if (myReqs != null)
                    {
                        var mR = db.MyRequests.ToList();
                        db.MyRequests.DeleteAllOnSubmit(mR);
                        db.SubmitChanges();
                    }

                    db.MyRequests.InsertAllOnSubmit(myRequests);
                    db.SubmitChanges();
                }
                #endregion

                #region myTreatments
                try
                {
                    var myTrtmns1 = db.MyTreatments.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<myTreatments>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }

                }
                var myTrtmns = db.MyTreatments.FirstOrDefault();
                myTreatments = await getAllMyTreatments(userID);

                if (myTreatments != null)
                {
                    foreach (var treatment in myTreatments)
                    {
                        if (treatment.Subject.Length > 45)
                        {
                            treatment.displayedSubject = treatment.Subject.Substring(0, 45) + "...";
                        }
                        else
                            treatment.displayedSubject = treatment.Subject;
                    }
                    for (int i = 0; i < myTreatments.Length; i++)
                    {
                        string[] date = myTreatments[i].LetterDate.Split(' ');
                        myTreatments[i].LetterDate = date[0];
                        date = myTreatments[i].TransactionDate.Split(' ');
                        myTreatments[i].TransactionDate = date[0];
                    }
                    if (myTrtmns != null)
                    {
                        var mT = db.MyTreatments.ToList();
                        db.MyTreatments.DeleteAllOnSubmit(mT);
                        db.SubmitChanges();
                    }

                    db.MyTreatments.InsertAllOnSubmit(myTreatments);
                    db.SubmitChanges();
                }

                #endregion

                #region poll
                try
                {
                    var pollT = db.PollTable.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<pollTable>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                try
                {
                    var pollAns = db.PollAnswersTable.FirstOrDefault();
                }
                catch (Exception ex)
                {

                    try
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<pollAnswersTable>();
                            databaseSchemaUpdater.Execute();
                        }
                        else
                            MessageBox.Show(ex.Message);
                    }
                    catch (Exception exc)
                    {

                        MessageBox.Show(exc.Message);
                    }
                }
                #region Caching Poll
                //var poll = db.PollTable.FirstOrDefault();
                //var pollAnsF = db.PollAnswersTable.FirstOrDefault();
                //poll_ = await getPoll();
                //if (poll_ != null)
                //{
                //    if (poll != null)
                //    {
                //        var p = db.PollTable.ToList();
                //        db.PollTable.DeleteAllOnSubmit(p);
                //        db.SubmitChanges();
                //    }
                //    if (pollAnsF != null)
                //    {
                //        var pAns = db.PollAnswersTable.ToList();
                //        db.PollAnswersTable.DeleteAllOnSubmit(pAns);
                //        db.SubmitChanges();
                //    }

                //    pollForSave = new pollTable();
                //    pollForSave.ID = poll_.ID;
                //    pollForSave.Question = poll_.Question;
                //    db.PollTable.InsertOnSubmit(pollForSave);
                //    db.SubmitChanges();

                //    answers = poll_.QuestionAnswers;

                //    foreach (var answer in answers)
                //    {
                //        pollAnswersForSave = new pollAnswersTable();
                //        pollAnswersForSave.ID = answer.ID;
                //        pollAnswersForSave.QuestionID = answer.QuestionID;
                //        pollAnswersForSave.Count = answer.Count;
                //        pollAnswersForSave.Answer = answer.Answer;
                //        pollAnswersForSave.pollID = pollForSave.ID;

                //        db.PollAnswersTable.InsertOnSubmit(pollAnswersForSave);
                //        db.SubmitChanges();
                //    }
                //}
                #endregion
                #endregion



                

            }

            #region insert, update and delete
            //requestTypes r = new requestTypes();         
            //r.RequestTypeName = "first";
            //db.RequestTypes.InsertOnSubmit(r);
            //db.SubmitChanges();

            //var first = (from r1 in db.RequestTypes
            //             select r1).FirstOrDefault();

            //MessageBox.Show(first.RequestTypeName);

            //first.RequestTypeName = "second";
            //db.SubmitChanges();

            //var second = (from r2 in db.RequestTypes
            //              select r2).FirstOrDefault();

            //MessageBox.Show(second.RequestTypeName);

            //db.RequestTypes.DeleteOnSubmit(second);
            //db.SubmitChanges();

            //var third = from r3 in db.RequestTypes
            //            select r3;

            //if (third==null)
            //{
            //    MessageBox.Show("deleted");
            //}
            //else
            //{
            //    MessageBox.Show(third.FirstOrDefault().RequestTypeName);
            //}

            #endregion
        }
    }
}