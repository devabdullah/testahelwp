﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Json;
using Windows.Devices.Geolocation;
using Microsoft.Phone.Data.Linq;
using System.Device.Location;
using System.Windows.Shapes;
using System.Windows.Media;
using Microsoft.Phone.Maps.Controls;
using System.Globalization;
using System.Threading;
using Newtonsoft.Json;
using System.IO.IsolatedStorage;

namespace Testahel_WP_v1._0
{
    public partial class ReqPage : PhoneApplicationPage
    {
        DataBaseContext db;
        districts[] allDistricts;
        allRoads[] roads;
        int DistrictID = -1, RoadID;
        bool IsLocationConfirmed = false;
        methods methods = new methods();

        requestTypes RecievedRequestType = new requestTypes();
        int RID;
        public ReqPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            codeInConstructor();
            ShowMyLocationOnTheMap();
        }
        
        private async void btnSave_Click_1(object sender, EventArgs e)
        {
            SystemTray.ProgressIndicator = new ProgressIndicator();
            methods.setProgressIndicator(true);
            SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
            SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
            SystemTray.ProgressIndicator.Text = "يتم إرسال الطلب";
            Geolocator gl = new Geolocator();
            gl.DesiredAccuracyInMeters = 50;
            Geoposition geoposition = await gl.GetGeopositionAsync(maximumAge: TimeSpan.FromMinutes(5), timeout: TimeSpan.FromSeconds(10));
            double Longitude = geoposition.Coordinate.Longitude;
            double Latitude = geoposition.Coordinate.Latitude;
            CultureInfo ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
            string CurrentDateTime = DateTime.Now.ToString("yyyy-MM-dd") + "T" + DateTime.Now.ToString("HH:mm:ss");
            ci = new CultureInfo("ar-SA");
            Thread.CurrentThread.CurrentCulture = ci;
            if (methods.checkNetworkConnection())
            {
                    string data = "<RequestArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><ERequests><Request><DistrictID>" + DistrictID + "</DistrictID><Latitude>" + Latitude + "</Latitude><LocationDescription>" + description.Text + "</LocationDescription><Longitude>" + Longitude + "</Longitude><NationalID>" + identityNum.Text + "</NationalID><RequestDate>" + CurrentDateTime + "</RequestDate><RequestStatus></RequestStatus><RequestTypeID>" + RID + "</RequestTypeID><RoadID>" + RoadID + "</RoadID></Request></ERequests></RequestArray>";
                    string url = methods.serverURL + "AddRequest";

                    if (IsLocationConfirmed)
                    {
                        string result = await methods.PostDataToServer(data, url, "application/xml");
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                        if (result != "")
                        {
                            var r = JsonConvert.DeserializeObject<postResult>(result);
                            if (r.StatusCode == 200)
                            {

                                MessageBox.Show("تم إستقبال طلبك وسيتم مراجعته من قبل المختصين وسوف يتم تزويدك برقم الطلب في وقت لاحق");
                                var settings = IsolatedStorageSettings.ApplicationSettings;
                                if (settings.Contains("addRequest"))
                                {
                                    settings["addRequest"] = "true";
                                }
                                else
                                {
                                    settings.Add("addRequest", "true");
                                }
                                settings.Save();
                                NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                            }
                            else
                            {
                                MessageBox.Show(r.StatusMessage);
                            }
                        }
                        else
                        {
                            MessageBox.Show(methods.errorMessage);
                        }
                    }
                    else
                    {
                        methods.setProgressIndicator(false);
                        SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                        MessageBox.Show("من فضلك إضغط على زر تحديد الموقع");

                    }
            }
            else
            {
                try
                {
                    if (IsLocationConfirmed)
                    {
                        var db = new DataBaseContext(DataBaseContext.DBConnectionString);

                        try
                        {
                            var AddedRequestTest = db.AllAddRequests.FirstOrDefault();
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.StartsWith("The specified table does not exist."))
                            {
                                DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                                databaseSchemaUpdater.AddTable<AddRequest>();
                                databaseSchemaUpdater.Execute();
                            }
                        }
                        var AddedRequest = db.AllAddRequests.FirstOrDefault();

                        if (AddedRequest != null)
                        {
                            var r = db.AllAddRequests.ToList();
                            db.AllAddRequests.DeleteAllOnSubmit(r);
                            db.SubmitChanges();
                        }
                        AddRequest AllAddedRequests = new AddRequest();
                        AllAddedRequests.DistrictID = DistrictID;
                        AllAddedRequests.Latitude = Latitude;
                        AllAddedRequests.LocationDescription = description.Text;
                        AllAddedRequests.Longitude = Longitude;
                        AllAddedRequests.NationalID = long.Parse(identityNum.Text);
                        AllAddedRequests.RequestDate = CurrentDateTime;
                        AllAddedRequests.RequestStatus = "";
                        AllAddedRequests.RequestTypeID = RID;
                        AllAddedRequests.RoadID = RoadID;
                        AddRequest[] item = { AllAddedRequests };
                        db.AllAddRequests.InsertAllOnSubmit(item);
                        db.SubmitChanges();
                        MessageBox.Show("تم حفظ طلبك و سوف يتم ارسال الطلب تلقائياً بمجرد اتصالك بالانترنت");
                        NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                    }
                    else
                    {
                        MessageBox.Show("من فضلك إضغط على زر تحديد الموقع");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("من فضلك تأكد من ادخال البيانات بشكل صحيح");
                }
            }
            ci = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = ci;
        }
        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            NavigationService.GoBack();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void lst_districts_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (lst_districts.SelectedItem != null)
            {
                districts d = lst_districts.SelectedItem as districts;
                DistrictID = d.ID;

                roads = db.AllRoads.Where(r => r.ParentID == DistrictID).ToArray();
                if (roads.Length == 0)
                {
                    string[] items = new string[1];
                    items[0] = "لا توجد شوارع بهذا الحي";
                    RoadID = 0;
                    lst_Streets.ItemsSource = items;

                    emptyStreets.Visibility = Visibility.Visible;
                    lst_Streets.Visibility = Visibility.Collapsed;
                }
                else
                {
                    lst_Streets.ItemsSource = roads;
                    lst_Streets.SelectedIndex = 0;

                    emptyStreets.Visibility = Visibility.Collapsed;
                    lst_Streets.Visibility = Visibility.Visible;
                    allRoads SpecifiedRoads = lst_Streets.SelectedItem as allRoads;
                    RoadID = SpecifiedRoads.ID;
                }

            }

        }

        private void lst_Streets_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void codeInConstructor()
        {
            if (methods.user.RegistrationTypeId==3)
            {
                txtID.Text = "رقم السجل التجاري";
                name.Text = methods.businessUser.Name;
                name.IsReadOnly = true;
                identityNum.Text = methods.businessUser.IdentityBuisinessNumber.ToString();
                identityNum.IsReadOnly = true;
                mobNum.Text = methods.businessUser.Mobile;
                mobNum.IsReadOnly = true;
            }
            else
            {
                if (methods.user.RegistrationTypeId == 1)
                    txtID.Text = "رقم السجل المدني";
                else
                    txtID.Text = "رقم الإقامة";
                name.Text = methods.individualUser.FirstName + " " + methods.individualUser.SecondName + " " + methods.individualUser.ThirdName + " " + methods.individualUser.FamilyName;
                name.IsReadOnly = true;
                identityNum.Text = methods.individualUser.IdentityNumber.ToString();
                identityNum.IsReadOnly = true;
                mobNum.Text = methods.individualUser.Mobile;
                mobNum.IsReadOnly = true;
            }
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            if (PhoneApplicationService.Current.State.ContainsKey("Request"))
            {
                RecievedRequestType = (requestTypes)PhoneApplicationService.Current.State["Request"];
            }
            tbPName.Text = RecievedRequestType.RequestTypeName;
            RID = RecievedRequestType.RequestTypeID;
            allDistricts = db.Districts.ToArray();



            lst_districts.ItemsSource = allDistricts;
        }

        private async void ShowMyLocationOnTheMap()
        {
            // Get my current location.
            Geolocator myGeolocator = new Geolocator();
            Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
            Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
            GeoCoordinate myGeoCoordinate = CoordinateConverter.ConvertGeocoordinate(myGeocoordinate);
            // Make my current location the center of the Map.
            this.mapWithMyLocation.Center = myGeoCoordinate;
            this.mapWithMyLocation.ZoomLevel = 13;
            // Create a small circle to mark the current location.
            Ellipse myCircle = new Ellipse();
            myCircle.Fill = new SolidColorBrush(Color.FromArgb(255, 130, 26, 24));
            myCircle.Height = 20;
            myCircle.Width = 20;
            myCircle.Opacity = 50;
            // Create a MapOverlay to contain the circle.
            MapOverlay myLocationOverlay = new MapOverlay();
            myLocationOverlay.Content = myCircle;
            myLocationOverlay.PositionOrigin = new Point(0.5, 0.5);
            myLocationOverlay.GeoCoordinate = myGeoCoordinate;
            // Create a MapLayer to contain the MapOverlay.
            MapLayer myLocationLayer = new MapLayer();
            myLocationLayer.Add(myLocationOverlay);
            // Add the MapLayer to the Map.
            mapWithMyLocation.Layers.Add(myLocationLayer);

        }

        private void btnSendMyLocation_Click_1(object sender, RoutedEventArgs e)
        {
            IsLocationConfirmed = true;
        }
    }
}