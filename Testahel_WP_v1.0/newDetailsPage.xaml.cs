﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;

namespace Testahel_WP_v1._0
{
    public partial class newDetailsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        notifications[] newFeeds = new notifications[1];
        public newDetailsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            notifications mobileFeed = new notifications();
            mobileFeed = (notifications)PhoneApplicationService.Current.State["mobileFeed"];
            newFeeds[0] = new notifications();
            newFeeds[0] = mobileFeed;
            lls_newDetails.ItemsSource = newFeeds; 
        }

        private async void details_Click_1(object sender, RoutedEventArgs e)
        {
            string url = "http://www.najran.gov.sa/"+newFeeds[0].NewsURL;
            await Launcher.LaunchUriAsync(new Uri(url));
        }
    }
}