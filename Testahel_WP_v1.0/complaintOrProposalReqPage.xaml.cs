﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.IO;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using System.Net.Http;
using System.Text;
using Microsoft.Phone.Data.Linq;
using System.Windows.Media;
using Newtonsoft.Json;
using System.Xml;

namespace Testahel_WP_v1._0
{
    public partial class complaintOrProposalReqPage : PhoneApplicationPage
    {
        methods methods = new methods();
        BitmapImage ChosenImage;
        string TypeName = string.Empty;
        string imgStreamString;
        public complaintOrProposalReqPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            setData();
        }

        private void type_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (type.SelectedIndex == 0)
            {
                //txtName.Text = "إسم المشتكي";
                //txtMail.Text = "بريد المشتكي";
                //txtNo.Text = "رقم هوية المشتكي";
                //txtMobNo.Text = "رقم جوال المشتكي";
                //txtPhoneNo.Text = "رقم هاتف المشتكي";
                //txtAdd.Text = "عنوان المشتكي";
                //txtBox.Text = "صندوق بريد المشتكي";
                txtAdd2.Text = "عنوان الشكوى";
                txtText.Text = "نص الشكوى";
            }
            else if (type.SelectedIndex == 1)
            {
                //txtName.Text = "إسم المقترح";
                //txtMail.Text = "بريد المقترح";
                //txtNo.Text = "رقم هوية المقترح";
                //txtMobNo.Text = "رقم جوال المقترح";
                //txtPhoneNo.Text = "رقم هاتف المقترح";
                //txtAdd.Text = "عنوان المقترح";
                //txtBox.Text = "صندوق بريد المقترح";
                txtAdd2.Text = "عنوان الإقتراح";
                txtText.Text = "نص الإقتراح";
            }
            else
            {
                //txtName.Text = "إسم المبلغ";
                //txtMail.Text = "بريد المبلغ";
                //txtNo.Text = "رقم هوية المبلغ";
                //txtMobNo.Text = "رقم جوال المبلغ";
                //txtPhoneNo.Text = "رقم هاتف المبلغ";
                //txtAdd.Text = "عنوان المبلغ";
                //txtBox.Text = "صندوق بريد المبلغ";
                txtAdd2.Text = "عنوان البلاغ";
                txtText.Text = "نص البلاغ";
            }

        }

        private async void btnSave_Click_1(object sender, EventArgs e)
        {

            byte[] ImageArray;
            string ImageAttachment;
            if (imgToBeSent.Source == null)
            {
                ImageAttachment = string.Empty;

                ImageArray = new byte[0];
            }
            else
            {
                ImageArray = methods.ImageToArray(ChosenImage);
                ImageAttachment = Convert.ToBase64String(ImageArray);
            }

            if (type.SelectedIndex == 0)
            {
                TypeName = "شكوي";
            }
            else if (type.SelectedIndex == 1)
            {
                TypeName = "إقتراح";
            }
            else if (type.SelectedIndex == 2)
            {
                TypeName = "بلاغ";
            }

            bool validation = validate();
            if (validation)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
                methods.setProgressIndicator(true);
                SystemTray.ProgressIndicator.Text = "يتم إرسال ال" + TypeName;
                SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                if (methods.checkNetworkConnection())
                {
                    string data = "<ContactUsArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><ContactUs><ContactUs><Address>" + address.Text + "</Address><Email>" + eMail.Text + "</Email><Fullname>" + fullName.Text + "</Fullname><LetterDetails>" + text.Text + "</LetterDetails><LetterTypeName>" + TypeName + "</LetterTypeName><MobileNo>" + mobNum.Text + "</MobileNo><NationalId>" + num.Text + "</NationalId><PObox>" + poBox.Text + "</PObox><TelephoneNo>" + phoneNum.Text + "</TelephoneNo><Title>" + txtAdd2.Text + "</Title><attachments>" + ImageAttachment + "</attachments></ContactUs></ContactUs></ContactUsArray>";
                    string url = methods.serverURL + "AddContactUs";
                    //url = "http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/AddContactUs";
                    string result = await methods.PostDataToServer(data, url, "application/xml");
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                    if (result != "" && (!result.Contains("error")||!result.Contains("Error")))
                    {
                        try
                        {
                            var r = JsonConvert.DeserializeObject<postResult>(result);
                            if (r.StatusCode == 200)
                            {
                                MessageBox.Show("تم تسجيل ال" + TypeName + " بنجاح");
                                NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.Relative));
                            }
                            else
                            {
                                MessageBox.Show(r.StatusMessage);
                            }
                        }
                        catch (Exception)
                        {

                            MessageBox.Show(methods.errorMessage);
                        }
                        
                    }
                    else
                    {
                        MessageBox.Show(methods.errorMessage);
                    }
                }
                else
                {
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);

                    var db = new DataBaseContext(DataBaseContext.DBConnectionString);

                    try
                    {
                        var Contact = db.AllContactUs.FirstOrDefault();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.StartsWith("The specified table does not exist."))
                        {
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<ContactUs>();
                            databaseSchemaUpdater.Execute();
                        }
                    }
                    var ContUs = db.AllContactUs.FirstOrDefault();
                    if (ContUs != null)
                    {
                        var r = db.AllContactUs.ToList();
                        db.AllContactUs.DeleteAllOnSubmit(r);
                        db.SubmitChanges();
                    }
                    var contactUs = new ContactUs();
                    contactUs.Address = address.Text;
                    contactUs.Email = eMail.Text;
                    contactUs.FullName = fullName.Text;
                    contactUs.LetterDetails = text.Text;
                    contactUs.LetterTypeName = TypeName;
                    contactUs.NationalID = long.Parse(num.Text);
                    contactUs.MobileNo = long.Parse(mobNum.Text);
                    contactUs.PObox = poBox.Text;
                    contactUs.TelephoneNo = long.Parse(phoneNum.Text);
                    contactUs.Title = txtAdd2.Text;
                    contactUs.attachments = ImageArray;
                    ContactUs[] ContactArr = { contactUs };
                    db.AllContactUs.InsertAllOnSubmit(ContactArr);
                    db.SubmitChanges();
                    MessageBox.Show("تم حفظ " + TypeName + " و سوف يتم ارسال الطلب تلقائياً بمجرد اتصالك بالانترنت");
                    NavigationService.Navigate(new Uri("/homePage.xaml", UriKind.RelativeOrAbsolute));
                }
            }
            else
            {
                MessageBox.Show("من فضلك تأكد من البيانات التى قمت بإدخالها");
            }
        }

        private void btnCancel_Click_1(object sender, EventArgs e)
        {
            if (NavigationService.CanGoBack)
            {
                NavigationService.GoBack();
            }
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        public bool validate()
        {
            methods r = new methods();
            bool title_isEmpty = r.isEmpty(reqTitle.Name, this);
            bool text_isEmpty = r.isEmpty(text.Name, this);
            if (!title_isEmpty && !text_isEmpty)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            PhotoChooserTask choser = new PhotoChooserTask();
            choser.ShowCamera = true;
            choser.Completed += choser_Completed;
            choser.Show();
        }

        void choser_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                var img = new BitmapImage();
                img.SetSource(e.ChosenPhoto);
                ChosenImage = img;
                imgToBeSent.Source = img;
            }
        }

        private void setData()
        {
            if (methods.user != null)
            {
                if (methods.user.RegistrationTypeId == 3)
                {
                    if (methods.businessUser != null)
                    {
                        txtID.Text = "رقم السجل التجاري";
                        fullName.Text = methods.businessUser.Name;
                        fullName.IsReadOnly = true;
                        eMail.Text = methods.businessUser.Email;
                        eMail.IsReadOnly = true;
                        num.Text = methods.businessUser.IdentityBuisinessNumber.ToString();
                        num.IsReadOnly = true;
                        mobNum.Text = methods.businessUser.Mobile;
                        mobNum.IsReadOnly = true;
                        phoneNum.Text = methods.businessUser.Telephone;
                        phoneNum.IsReadOnly = true;
                        address.Text = methods.businessUser.Address;
                        address.IsReadOnly = true;
                        poBox.Text = methods.businessUser.POBox;
                        poBox.IsReadOnly = true;
                    }
                }
                else
                {
                    if (methods.individualUser != null)
                    {
                        if (methods.user.RegistrationTypeId == 1)
                        {
                            txtID.Text = "رقم السجل المدني";
                        }
                        else
                        {
                            txtID.Text = "رقم الإقامة";
                        }
                        fullName.Text = methods.individualUser.FirstName + " " + methods.individualUser.SecondName + " " + methods.individualUser.ThirdName + " " + methods.individualUser.FamilyName;
                        fullName.IsReadOnly = true;
                        eMail.Text = methods.individualUser.Email;
                        eMail.IsReadOnly = true;
                        num.Text = methods.individualUser.IdentityNumber.ToString();
                        num.IsReadOnly = true;
                        mobNum.Text = methods.individualUser.Mobile;
                        mobNum.IsReadOnly = true;
                        phoneNum.Text = methods.individualUser.Telephone;
                        phoneNum.IsReadOnly = true;
                        address.Text = methods.individualUser.BirthPlace;
                        address.IsReadOnly = true;
                        poBox.Text = methods.individualUser.POBox;
                        poBox.IsReadOnly = true;
                    }
                }
            }
        }

    }
}