﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;
using System.Windows.Threading;
using System.Globalization;
using System.Threading;
using Microsoft.Phone.Data.Linq;
using System.Data.Linq;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Resources;
using System.IO.IsolatedStorage;
using Microsoft.Phone.Scheduler;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{
    public partial class homePage : PhoneApplicationPage
    {
        DataBaseContext db = new DataBaseContext(DataBaseContext.DBConnectionString);
        methods methods = new methods();
        poll poll_;
        #region Background Task Agent Variables (Do not touch or change)
        PeriodicTask periodicTask;
        ResourceIntensiveTask resourceIntensiveTask;

        string periodicTaskName = "PeriodicAgent";
        string resourceIntensiveTaskName = "ResourceIntensiveAgent";
        public bool agentsAreEnabled = true;
        bool ignoreCheckBoxEvents = false;
        #endregion
        public homePage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains("previousPage"))
            {
                if (settings["previousPage"]=="app")
                {
                    methods.getData();
                }
            }
            else
            {
                methods.getData();
            }
        }
        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            //myScrollViewer.Content = myCanvas;
            try
            {
                methods.setProgressIndicator(false);
                SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                StartPeriodicAgent();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        //void homePage_Loaded(object sender, RoutedEventArgs e)
        //{
            //try
            //{
            //    //new Uri("/Images/1.jpg", UriKind.RelativeOrAbsolute)
              // var LastPage = NavigationService.BackStack.FirstOrDefault();
            //    MessageBox.Show(LastPage.Source.ToString());

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.ToString());
            //}
            //methods.setDateValue(txtDate.Name, this); 
            //methods.getData();
        //}

        private void btn_req_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/complaintOrProposalReqPage.xaml", UriKind.Relative));
        }
                                   
        private void btn_req940_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/r940Page.xaml", UriKind.Relative));
        }

        private void btn_takyem_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/evaluationPage.xaml", UriKind.Relative));
        }

        private void btn_personalSettings_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/personalSettingsPage.xaml", UriKind.Relative));
        }

        private void btn_req_Click_2(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/typesofRequestsPage.xaml", UriKind.Relative));
        }

        private void btn_about_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/aboutNajranPage.xaml", UriKind.Relative));
        }

        private void btn_myRequests_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/myRequestsPage.xaml", UriKind.Relative));
        }

        private void btn_mine_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/mo3amlatyPage.xaml", UriKind.Relative));
        }

        private void btn_6ware2_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/emergencyGuidePage.xaml", UriKind.Relative));
        }

        private async void btn_openions_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (methods.checkNetworkConnection())
                {
                    bool IsPollAnswered = false;
                    bool DataExists = false;
                    try
                    {
                        var test = db.PollTable.FirstOrDefault();
                        DataExists = true;
                    }
                    catch (NullReferenceException x)
                    {
                        DataExists = false;
                    }
                    catch (Exception ex)
                    {
                        
                            DatabaseSchemaUpdater databaseSchemaUpdater = db.CreateDatabaseSchemaUpdater();
                            databaseSchemaUpdater.AddTable<pollTable>();
                            databaseSchemaUpdater.Execute();
                        
                    }
                    poll_ = await methods.getPoll();
                    if (DataExists)
                    {
                        List<pollTable> AllAnsweredPolls = db.PollTable.ToList();

                        if (AllAnsweredPolls.Count != 0)
                        {
                            foreach (var item in AllAnsweredPolls)
                            {
                                if (item.ID == poll_.ID)
                                {
                                    IsPollAnswered = true;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        IsPollAnswered = false;
                    }
                    PhoneApplicationService.Current.State["CurrentPoll"] = poll_;
                    var settings = IsolatedStorageSettings.ApplicationSettings;
                    settings["CurrentPoll"] = poll_;
                    settings.Save();
                    if (IsPollAnswered)
                    {

                        NavigationService.Navigate(new Uri("/PollsResult.xaml", UriKind.Relative));
                    }
                    else
                    {
                        NavigationService.Navigate(new Uri("/pollsPage.xaml", UriKind.Relative));
                    }
                }
                else
                {
                    MessageBox.Show("لا يوجد اتصال بالشبكة");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("تأكد من اختيار اجابة و الاتصال بالشبكة ثم حاول مرة أخري");
                //MessageBox.Show(ex.ToString());
            }
        }

        private void btn_lwa27_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Regulations.xaml", UriKind.Relative));
        }

        private void btnSettings_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/personalSettingsPage.xaml", UriKind.Relative));
        }

        private async void btn_gate_Click_1(object sender, RoutedEventArgs e)
        {
            await Launcher.LaunchUriAsync(new Uri("http://www.najran.gov.sa/Registration/Pages/default.aspx"));
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/helpPage.xaml", UriKind.Relative));
        }

        private void btn_notification_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/notificationsPage.xaml", UriKind.Relative));
        }

        private void btn_Myinfo_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/viewProfilePage.xaml", UriKind.Relative));
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.Terminate();
        }

        private void ApplicationBarMenuItem_Click_1(object sender, EventArgs e)
        {
            var loginData = db.LoginData.ToList();
            var userData = db.IndividualUserData.ToList();
            var businessUser = db.BusinessSectorData.ToList();
            var myRequests = db.MyRequests.ToList();
            var requests = db.MyRequests.ToArray();
            var myTreatments = db.MyTreatments.ToList();
            var myT1 = db.MyTreatments.FirstOrDefault();
            if (myTreatments.Count!=0)
            {
                db.MyTreatments.DeleteAllOnSubmit(myTreatments);
                db.SubmitChanges();
            }
            var myT = db.MyTreatments.FirstOrDefault();
            var myR1 = db.MyRequests.FirstOrDefault();
            if (myRequests.Count!=0)
            {
                db.MyRequests.DeleteAllOnSubmit(myRequests);
                db.SubmitChanges();
            }
            var myR = db.MyRequests.FirstOrDefault();
            if (loginData.Count!=0)
            {
                db.LoginData.DeleteAllOnSubmit(loginData);
                db.SubmitChanges();
            }
            if (userData.Count!=0)
            {
                db.IndividualUserData.DeleteAllOnSubmit(userData);
                db.SubmitChanges();
            }
            if (businessUser.Count!=0)
            {
                db.BusinessSectorData.DeleteAllOnSubmit(businessUser);
                db.SubmitChanges();
            }
            var settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains("isLogin"))
            {
                settings["isLogin"] = "false";
            }
            else
            {
                settings.Add("isLogin", "false");
            }
            settings.Save();
            if (settings.Contains("previousPage"))
            {
                settings["previousPage"] = "main";
            }
            else
            {
                settings.Add("previousPage", "main");
            }
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            //Application.Current.Terminate();
        }

        #region Background Task Agent Methods (Do not touch or edit)
        private void StartPeriodicAgent()
        {
            // Variable for tracking enabled status of background agents for this app.
            agentsAreEnabled = true;

            // Obtain a reference to the period task, if one exists
            periodicTask = ScheduledActionService.Find(periodicTaskName) as PeriodicTask;

            // If the task already exists and background agents are enabled for the
            // application, you must remove the task and then add it again to update 
            // the schedule
            if (periodicTask != null)
            {
                RemoveAgent(periodicTaskName);
            }

            periodicTask = new PeriodicTask(periodicTaskName);

            // The description is required for periodic agents. This is the string that the user
            // will see in the background services Settings page on the device.
            periodicTask.Description = "This demonstrates a periodic task.";

            // Place the call to Add in a try block in case the user has disabled agents.
            
        }
       
        private void RemoveAgent(string name)
        {
            try
            {
                ScheduledActionService.Remove(name);
            }
            catch (Exception)
            {
            }
        }
        #endregion

        private void btn_News_Click_1(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/newsPage.xaml", UriKind.Relative));
        }

        private async void btn_najran_Click_1(object sender, RoutedEventArgs e)
        {
            await Launcher.LaunchUriAsync(new Uri("http://www.najran.gov.sa/Registration/Pages/default.aspx"));
        }
    }
}