﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Notification;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Windows.Media;

namespace Testahel_WP_v1._0
{

    public partial class notificationsPage : PhoneApplicationPage
    {
        DataBaseContext db;
        tenderAnnouncements[] tenderAnnouncements;
        investmentAnnouncements[] investmentAnnouncements;
        methods methods = new methods();
        public string _summary;
        public string imageSource;

        public notificationsPage()
        {
            InitializeComponent();
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
            getNotifications();
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private async void _getNotifications()
        {
            //SystemTray.ProgressIndicator = new ProgressIndicator();
            //SystemTray.ProgressIndicator.Text = "يتم تحميل الأخبار";
            //SystemTray.ProgressIndicator.IsIndeterminate = true;
            //SystemTray.ProgressIndicator.IsVisible = true;
            if (methods.checkNetworkConnection())
            {

                #region Tender Announcements
                tenderAnnouncements = await methods.getTenderAnnouncements();
                lls_announcements.ItemsSource = tenderAnnouncements;
                #endregion

                #region Investment Announcements
                investmentAnnouncements = await methods.getInvestmentAnnouncements();
                if (investmentAnnouncements!=null)
                {
                    for (int i = 0; i < investmentAnnouncements.Length; i++)
                    {
                        if (investmentAnnouncements[i].TenderTitle.Length>20)
                        {
                            investmentAnnouncements[i].displayedTitle = investmentAnnouncements[i].TenderTitle.Substring(0, 20) + "...";
                        }
                        else
                        {
                            investmentAnnouncements[i].displayedTitle = investmentAnnouncements[i].TenderTitle;
                        }
                    }
                }
                lls_chances.ItemsSource = investmentAnnouncements;
                #endregion

            }
            else
            {
                MessageBox.Show(methods.internetConnectionFailedMessage);
            }
            
            //SystemTray.ProgressIndicator.IsIndeterminate = false;
            //SystemTray.ProgressIndicator.IsVisible = false;

        }

        private  void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            
        }

        private void lls_announcements_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {

            tenderAnnouncements tenderAnnouncement = (tenderAnnouncements)lls_announcements.SelectedItem;
            if (tenderAnnouncement != null)
            {
                PhoneApplicationService.Current.State["tenderAnnouncements"] = tenderAnnouncement;
                NavigationService.Navigate(new Uri("/tenderAnnouncementDetailsPage.xaml", UriKind.Relative));
            }
            
        }

        private void lls_chances_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            investmentAnnouncements investmentAnnouncement = (investmentAnnouncements)lls_chances.SelectedItem;
            if (investmentAnnouncement != null)
            {
                PhoneApplicationService.Current.State["investmentAnnouncement"] = investmentAnnouncement;
                NavigationService.Navigate(new Uri("/investmentAnnouncementDetailsPage.xaml", UriKind.Relative));
            }
            
        }

        private async void getNotifications()
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            var checkedItems = db.CheckedNotifications.Where(n => n.checkedItem == true).ToList();
            var unCheckedItems = db.CheckedNotifications.Where(n => n.checkedItem == false).ToList();
            if (checkedItems.Count != 0)
            {
                if (methods.checkNetworkConnection())
                {
                    if (lls_announcements.ItemsSource == null || lls_chances.ItemsSource == null)
                    {
                        SystemTray.ProgressIndicator = new ProgressIndicator();
                        methods.setProgressIndicator(true);
                        SystemTray.ProgressIndicator.Text = "يتم تحميل طلباتي";
                        SystemTray.BackgroundColor = Color.FromArgb(255, 130, 26, 24);
                        SystemTray.ForegroundColor = Color.FromArgb(255, 247, 250, 223);
                    }
                    foreach (var item in checkedItems)
                    {
                        //TextBlock tb = this.FindName("txt_" + item.name) as TextBlock;
                        //TextBlock tbR = this.FindName("txtRemove_" + item.name) as TextBlock;
                        //LongListSelector lls = this.FindName("lls_" + item.name) as LongListSelector;
                        ////lls.Visibility = Visibility.Visible;
                        //tb.Visibility = Visibility.Collapsed;
                        //tbR.Visibility = Visibility.Collapsed;
                        if (item.name == "announcements")
                        {
                            tenderAnnouncements = await methods.getTenderAnnouncements();
                            if (tenderAnnouncements.Count() != 0)
                            {
                                lls_announcements.ItemsSource = tenderAnnouncements;
                                txt_announcements.Visibility = Visibility.Collapsed;
                                txtRemove_announcements.Visibility = Visibility.Collapsed;
                                lls_announcements.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                txt_announcements.Visibility = Visibility.Visible;
                                txt_announcements.Text = "لا توجد إشعارات";
                                txtRemove_announcements.Visibility = Visibility.Collapsed;
                                lls_announcements.Visibility = Visibility.Collapsed;
                            }
                            
                        }
                        else if (item.name == "chances")
                        {
                            investmentAnnouncements = await methods.getInvestmentAnnouncements();
                            if (investmentAnnouncements.Count() != 0)
                            {
                                for (int i = 0; i < investmentAnnouncements.Length; i++)
                                {
                                    if (investmentAnnouncements[i].TenderTitle.Length > 20)
                                    {
                                        investmentAnnouncements[i].displayedTitle = investmentAnnouncements[i].TenderTitle.Substring(0, 20) + "...";
                                    }
                                    else
                                    {
                                        investmentAnnouncements[i].displayedTitle = investmentAnnouncements[i].TenderTitle;
                                    }
                                }
                                lls_chances.ItemsSource = investmentAnnouncements;
                                txt_chances.Visibility = Visibility.Collapsed;
                                txtRemove_chances.Visibility = Visibility.Collapsed;
                                lls_chances.Visibility = Visibility.Visible;
                            }
                            else
                            {
                                txt_chances.Visibility = Visibility.Visible;
                                txt_chances.Text = "لا توجد إشعارات";
                                txtRemove_chances.Visibility = Visibility.Collapsed;
                                lls_chances.Visibility = Visibility.Collapsed;
                            }
                            
                        }
                    }
                    methods.setProgressIndicator(false);
                    SystemTray.BackgroundColor = Color.FromArgb(255, 247, 250, 223);
                    SystemTray.ForegroundColor = Color.FromArgb(255, 130, 26, 24);
                }
                else
                {
                    foreach (var item in checkedItems)
                    {
                        if (item.name == "chances")
                        {
                            txt_chances.Visibility = Visibility.Visible;
                            txt_chances.Text = methods.internetConnectionFailedMessage;
                            txtRemove_chances.Visibility = Visibility.Collapsed;
                            lls_chances.Visibility = Visibility.Collapsed;
                        }
                        if (item.name == "announcements")
                        {
                            txt_announcements.Visibility = Visibility.Visible;
                            txt_announcements.Text = methods.internetConnectionFailedMessage;
                            txtRemove_announcements.Visibility = Visibility.Collapsed;
                            lls_announcements.Visibility = Visibility.Collapsed;
                        }
                    }
                }

            }
            if (unCheckedItems.Count != 0)
            {
                foreach (var item in unCheckedItems)
                {
                    if (item.name == "chances")
                    {
                        txt_chances.Visibility = Visibility.Collapsed;
                        txtRemove_chances.Visibility = Visibility.Visible;
                        lls_chances.Visibility = Visibility.Collapsed;
                    }
                    if (item.name == "announcements")
                    {
                        txt_announcements.Visibility = Visibility.Collapsed;
                        txtRemove_announcements.Visibility = Visibility.Visible;
                        lls_announcements.Visibility = Visibility.Collapsed;
                    }
                }
            }
        }
    }
}