﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Globalization;
using System.Threading;

namespace Testahel_WP_v1._0
{
    public partial class myRequestDetailsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        myRequests[] mine = new myRequests[1];
        myRequests myRequest = new myRequests();
        public myRequestDetailsPage()
        {
            InitializeComponent();
            CultureInfo ci = new CultureInfo("ar-SA");
            Thread.CurrentThread.CurrentCulture = ci;
            methods.setDateValue(txtDate);
            tbWel.Text += methods.userName;
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            myRequest = (myRequests)PhoneApplicationService.Current.State["myRequest"];   
            if (myRequest.Attachments == null)
            {
                myRequest.Attachments = "لا توجد ملحقات مع هذا الطلب";
            }
            if (myRequest.RequestStatus == "1")
            {
                myRequest.RequestStatus = "مقبول";
            }
            else if (myRequest.RequestStatus == "0")
            {
                myRequest.RequestStatus = "مرفوض";
            }
            if (myRequest.Detail==null)
            {
                myRequest.Detail = "لا توجد تفاصيل";
            }
            //string[] date = myRequest.RequestDate.Split(' ');
            DateTime d = Convert.ToDateTime(myRequest.RequestDate);
            myRequest.RequestDate = d.Date.ToString("dd/MM/yyyy");
            mine[0] = new myRequests();
            mine[0] = myRequest;
            lls_myRequestDetails.ItemsSource = mine;
        }                   
    }
}