﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Windows.System;
using Microsoft.Phone.Tasks;
using System.Text;
using Newtonsoft.Json.Linq;

namespace Testahel_WP_v1._0
{
    public partial class displayAndDownloadRegulationsPage : PhoneApplicationPage
    {
        methods methods = new methods();
        public displayAndDownloadRegulationsPage()
        {
            InitializeComponent();
            //methods.setDateValue(txtDate.Name, this);
            //tbWel.Text += methods.userName;
            this.Loaded += displayAndDownloadRegulationsPage_Loaded;
            //http://najran.qadirgroup.net/qadirgroup.net/najran/MobileService.svc/GetAllRegulations
        }

        void displayAndDownloadRegulationsPage_Loaded(object sender, RoutedEventArgs e)
        {
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            client.DownloadStringAsync(new Uri("http://najran.qadirgroup.net/qadirgroup.net/najran/MobileService.svc/GetAllRegulations", UriKind.RelativeOrAbsolute));
            client.DownloadStringCompleted += client_DownloadStringCompleted;
        }

        void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            string response = e.Result.ToString();
            JArray a = JArray.Parse(response);
            foreach (JObject o in a.Children<JObject>())
            {
                foreach (JProperty p in o.Properties())
                {
                    string name = p.Name;
                    string value = p.Value.ToString();
                    MessageBox.Show(name + " : " + value);
                }
            }
        }

        private async void file1_Click_1(object sender, RoutedEventArgs e)
        {
            await Launcher.LaunchUriAsync(new Uri("https://www.google.com.eg/"));
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void btnDownload_Click_1(object sender, RoutedEventArgs e)
        {
            WebBrowserTask browser = new WebBrowserTask();
            browser.URL = "https://www.dropbox.com/s/pmyb69fb0ssdmkn/Scalable%20and%20Modular%20Architecture%20to%20CSS.pdf";
            browser.Show();
        }
        public class RootObject
        {
            public string Description { get; set; }
            public int ID { get; set; }
            public string Link { get; set; }
            public string Name { get; set; }
        }
      //  private WebClient wc = new WebClient();
        //private async void viewPDF_Click_1(object sender, RoutedEventArgs e)
        //{
        //    //wc.OpenReadCompleted += wc_OpenReadCompleted;
        //    //wc.OpenReadAsync(new Uri("http://www.blhr.org/media/documents/test.pdf"));

        //    //await Launcher.LaunchUriAsync(new Uri("https://www.dropbox.com/s/pmyb69fb0ssdmkn/Scalable%20and%20Modular%20Architecture%20to%20CSS.pdf"));

        //    webBrowserForBDF.Source = new Uri("https://www.dropbox.com/s/pmyb69fb0ssdmkn/Scalable%20and%20Modular%20Architecture%20to%20CSS.pdf", UriKind.RelativeOrAbsolute);

        //    //var resource = Application.GetResourceStream(new Uri("Testahel_WP_v1.0;component/pdfFile.pdf", UriKind.Relative));

        //    //this.pdf1.LoadDocument(resource.Stream);
        //    //pdf1.Visibility = Visibility.Visible;
        //}

        //void wc_OpenReadCompleted(object sender, OpenReadCompletedEventArgs e)
        //{
        //    pdf1.LoadDocument(e.Result);
        //    pdf1.Visibility = Visibility.Visible;
        //}
    }
}