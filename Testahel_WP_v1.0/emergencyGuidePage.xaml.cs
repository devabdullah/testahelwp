﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;

namespace Testahel_WP_v1._0
{
    
    public partial class emergencyGuidePage : PhoneApplicationPage
    {
        methods methods = new methods();
        emergencyGuid[] emergencyGuid;
        DataBaseContext db;
        public emergencyGuidePage()
        {
            InitializeComponent();
            tbWel.Text += methods.userName;
            methods.setDateValue(txtDate);           
        }

        private void btnHelp_Click_1(object sender, EventArgs e)
        {

        }

        private void myContacts_Tap_1(object sender, System.Windows.Input.GestureEventArgs e)
        {
            emergencyGuid em = (emergencyGuid)myContacts.SelectedItem;
            PhoneCallTask call = new PhoneCallTask();
            call.PhoneNumber = em.Telephone.ToString(); ;
            call.Show();
        }

        private async void PhoneApplicationPage_Loaded_1(object sender, RoutedEventArgs e)
        {
            db = new DataBaseContext(DataBaseContext.DBConnectionString);
            emergencyGuid = db.EmergencyGuid.ToArray();
            myContacts.ItemsSource = emergencyGuid;
        }
    }
}