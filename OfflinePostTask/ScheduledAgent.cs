﻿using System.Diagnostics;
using System.Windows;
using Microsoft.Phone.Scheduler;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Phone.Data.Linq;
using System.Net;
using System;
using Microsoft.Phone.Net.NetworkInformation;
using Testahel_WP_v1._0;
using Microsoft.Phone.Shell;

namespace OfflinePostTask
{
    public class ScheduledAgent : ScheduledTaskAgent
    {
        /// <remarks>
        /// ScheduledAgent constructor, initializes the UnhandledException handler
        /// </remarks>
        static ScheduledAgent()
        {
            // Subscribe to the managed exception handler
            Deployment.Current.Dispatcher.BeginInvoke(delegate
            {
                Application.Current.UnhandledException += UnhandledException;
            });
        }
        DataBaseContext db = new DataBaseContext(DataBaseContext.DBConnectionString);
        /// Code to execute on Unhandled Exceptions
        private static void UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }
        #region General Data Post
        public Task<string> PostDataToServer(string url, string DataToSend)
        {
            var tcs = new TaskCompletionSource<string>();
            var client = new WebClient();
            client.Headers["Content-Type"] = "application/xml";
            UploadStringCompletedEventHandler h = null;
            h = (sender, args) =>
            {
                if (args.Cancelled)
                {
                    tcs.SetCanceled();
                }
                else if (args.Error != null)
                {
                    tcs.SetException(args.Error);
                }
                else
                {
                    tcs.SetResult(args.Result);
                }

                client.UploadStringCompleted -= h;
            };

            client.UploadStringCompleted += h;
            client.UploadStringAsync(new Uri(url, UriKind.RelativeOrAbsolute), "POST", DataToSend);
            return tcs.Task;
        }
        #endregion
        #region Add Request
        public async void PostRequest()
        {
            var lstRequests = db.AllAddRequests.ToList();
            if (lstRequests != null || lstRequests.Count != 0)
            {
                string AllAddRequests = "";
                for (int i = 0; i < lstRequests.Count; i++)
                {
                    AllAddRequests += "<Request><DistrictID>" + lstRequests[i].DistrictID + "</DistrictID><Latitude>" + lstRequests[i].Latitude + "</Latitude><LocationDescription>" + lstRequests[i].LocationDescription + "</LocationDescription><Longitude>" + lstRequests[i].Longitude + "</Longitude><NationalID>" + lstRequests[i].NationalID + "</NationalID><RequestDate>" + lstRequests[i].RequestDate + "</RequestDate><RequestStatus></RequestStatus><RequestTypeID>" + lstRequests[i].RequestTypeID + "</RequestTypeID><RoadID>" + lstRequests[i].RoadID + "</RoadID></Request>";
                }
                string FullStringToBePosted = "<RequestArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><ERequests>" + AllAddRequests + "</ERequests></RequestArray>";
                string Result = await PostDataToServer("http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/AddRequest", FullStringToBePosted);
                if (Result.Contains("1"))
                {
                    db.AllAddRequests.DeleteAllOnSubmit(lstRequests);
                    db.SubmitChanges();
                }
            }
        }
        #endregion
        #region Add Evaluation
        public async void PostEvaluation()
        {
            var lstEvals = db.Evaluations.ToList();
            if (lstEvals != null || lstEvals.Count != 0)
            {
                string DataToBeSent = "";
                for (int i = 0; i < lstEvals.Count; i++)
                {
                    DataToBeSent += "<Evaluation><IsSatisfied>" + lstEvals[i].IsSatisfied.ToString().ToLower() + "</IsSatisfied><NationalID>" + lstEvals[i].NationalID + "</NationalID><Reasons>" + lstEvals[i].Reasons + "</Reasons><Suggestions>" + lstEvals[i].Suggestions + "</Suggestions>";
                }
                string FullStringToBePosted = "<EvaluationArray xmlns=\"http://schemas.datacontract.org/2004/07/MobileService.Models\"><evaluations>" + DataToBeSent + "</evaluations></EvaluationArray>";
                string result = await PostDataToServer("http://najran.qadirgroup.net/qadirgroup.net/najran/MobileService.svc/AddEvaluation", FullStringToBePosted);
                if (result != "0")
                {
                    db.Evaluations.DeleteAllOnSubmit(lstEvals);
                    db.SubmitChanges();
                }
            }

        }
        #endregion
        #region Send Complaint
        public async void PostComplaints()
        {
            var lstComplaints = db.AllContactUs.ToList();
            if (lstComplaints.Count != 0 || lstComplaints != null)
            {
                string DataToBeSent = "";

                for (int i = 0; i < lstComplaints.Count; i++)
                {
                    DataToBeSent += "<ContactUs><Address>" + lstComplaints[i].Address + "</Address><Email>" + lstComplaints[i].Email + "</Email><Fullname>" + lstComplaints[i].FullName + "</Fullname><LetterDetails>" + lstComplaints[i].LetterDetails + "</LetterDetails><LetterTypeName>" + lstComplaints[i].LetterTypeName + "</LetterTypeName><MobileNo>" + lstComplaints[i].MobileNo + "</MobileNo><NationalId>" + lstComplaints[i].NationalID + "</NationalId><PObox>" + lstComplaints[i].PObox + "</PObox><TelephoneNo>" + lstComplaints[i].TelephoneNo + "</TelephoneNo><Title>" + lstComplaints[i].Title + "</Title><attachments>" + Convert.ToBase64String(lstComplaints[i].attachments) + "</attachments></ContactUs>";
                }
                string FullStringToBePosted = "<ContactUsArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><ContactUs>" + DataToBeSent + " </ContactUs></ContactUsArray>";
                string result = await PostDataToServer("http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/AddContactUs", FullStringToBePosted);
                if (result.Length > 0)
                {
                    db.AllContactUs.DeleteAllOnSubmit(lstComplaints);
                    db.SubmitChanges();
                }
            }
        }
        #endregion
        #region Send Incident
        public async void PostIncidents()
        {
            var lstIncidents = db.AllIncidents.ToList();
            if (lstIncidents.Count != 0 || lstIncidents != null)
            {
                string DataToBeSent = "";
                for (int i = 0; i < lstIncidents.Count; i++)
                {
                    DataToBeSent += "<Incident><CallerNumber>" + lstIncidents[i].CallerNumber + "</CallerNumber><DeptID>" + lstIncidents[i].DeptID + "</DeptID><DistrictID>" + lstIncidents[i].DistrictID + "</DistrictID><DistrictName>" + lstIncidents[i].DistrictName + "</DistrictName><IncidentDescription>" + lstIncidents[i].IncidentDescription + "</IncidentDescription><Latitude>" + lstIncidents[i].Latitude + "</Latitude><LocationDescription>" + lstIncidents[i].LocationDescription + "</LocationDescription><Longitude>" + lstIncidents[i].Longitude + "</Longitude><NationalID>" + lstIncidents[i].NationalID + "</NationalID><RoadID>+" + lstIncidents[i].RoadID + "</RoadID><RoadName>" + lstIncidents[i].RoadName + "</RoadName><TypeID>" + lstIncidents[i].TypeID + "</TypeID></Incident>";
                }
                string FullStringToBeSent = "<IncidentArray xmlns=\"http://schemas.datacontract.org/2004/07/TestahelService.Classes\"><Incidents>" + DataToBeSent + "</Incidents></IncidentArray>";
                string Result = await PostDataToServer("http://najran2.qadirgroup.net/qadirgroup.net/najran2/Service.svc/InsertIncident", FullStringToBeSent);
                if (Result.Contains("1"))
                {
                    db.AllIncidents.DeleteAllOnSubmit(lstIncidents);
                    db.SubmitChanges();
                }
            }
        }
        #endregion
        /// <summary>
        /// Agent that runs a scheduled task
        /// </summary>
        /// <param name="task">
        /// The invoked task
        /// </param>
        /// <remarks>
        /// This method is called when a periodic or resource intensive task is invoked
        /// </remarks>
        protected override void OnInvoke(ScheduledTask task)
        {

            //TODO: Add code to perform your task in background
            if (CheckNetworkConnection())
            {
                ShellToast toast = new ShellToast();
                toast.Content = "سيتم الآن ارسال الطلبات التي تم طلبها و انت غير متصل بالانترنت";
                toast.Title = "تنبيه!";
                toast.Show();
                PostRequest();
                PostEvaluation();
                PostComplaints();
                PostIncidents();
                toast.Content = "تم ارسال البيانات بنجاح";
                toast.Show();

                //            <Request><DistrictID>2147483647</DistrictID><Latitude>1.26743233E+15</Latitude><LocationDescription>String content</LocationDescription><Longitude>1.26743233E+15</Longitude><NationalID>9223372036854775807</NationalID><RequestDate>1999-05-31T11:20:00</RequestDate><RequestStatus>String content</RequestStatus><RequestTypeID>2147483647</RequestTypeID><RoadID>2147483647</RoadID></Request>
            }
            NotifyComplete();
        }

        public static bool CheckNetworkConnection()
        {
            var networkInterface = NetworkInterface.NetworkInterfaceType;

            bool isConnected = false;
            if ((networkInterface == NetworkInterfaceType.Wireless80211) || (networkInterface == NetworkInterfaceType.MobileBroadbandGsm) || (networkInterface == NetworkInterfaceType.MobileBroadbandCdma))
                isConnected = true;

            else if (networkInterface == NetworkInterfaceType.None)
                isConnected = false;
            return isConnected;
        }
    }
}