﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Linq.Mapping;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testahel_WP_v1._0
{

    [Table]
    public class login
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }

        [Column]
        public string userName { get; set; }

        [Column]
        public string password { get; set; }

    }

    [Table]
    public class requestTypes
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }

        [Column]
        public int RequestTypeID { get; set; }

        [Column]
        public string RequestTypeName { get; set; }
    }

    [Table]
    public class districts
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id_pk { get; set; }
        [Column]
        public int ID { get; set; }
        [Column]
        public string LabelArStr { get; set; }
    }

    [Table]
    public class allRoads
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id_pk { get; set; }
        [Column]
        public int ID { get; set; }
        [Column]
        public string LabelArStr { get; set; }
        [Column]
        public int ParentID { get; set; }
    }

    [Table]
    public class incidentCategories
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }
        [Column]
        public string ArabicName { get; set; }
        [Column]
        public int CategoryID { get; set; }
        [Column]
        public int OId { get; set; }
    }

    [Table]
    public class allIncidentTypes
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id_pk { get; set; }
        [Column]
        public string ArabicName { get; set; }
        [Column]
        public int CategoryID { get; set; }
        [Column]
        public int OId { get; set; }
    }

    [Table]
    public class streets
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public int ID { get; set; }

        [Column]
        public string LabelArStr { get; set; }
    }

    [Table]
    public class incidentCategoriesTypes
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public string ArabicName { get; set; }
        [Column]
        public int OId { get; set; }
    }

    [Table]
    public class notifications
    {
        [Column(IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public string ID { get; set; }
        [Column]
        public string Date { get; set; }
        [Column]
        public string Title { get; set; }
        [Column]
        public string displayedTitle { get; set; }
        [Column]
        public string Summary { get; set; }
        [Column]
        public string Details { get; set; }
        [Column]
        public string ImagePath { get; set; }
        [Column]
        public string NewsURL { get; set; }
    }

    [Table]
    public class tenderAnnouncements
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }
        [Column]
        public bool CanBuyFromPortal { get; set; }
        [Column]
        public string ModificationDate { get; set; }
        [Column]
        public int ProjectTypeID { get; set; }
        [Column]
        public string ProjectTypeName { get; set; }
        [Column]
        public string SubmissionPlaceName { get; set; }
        [Column]
        public string TenderCode { get; set; }
        [Column]
        public string TenderCost { get; set; }
        [Column]
        public string TenderDeliveryDate { get; set; }
        [Column]
        public string TenderDetails { get; set; }
        [Column]
        public int TenderID { get; set; }
        [Column]
        public string TenderOpenDate { get; set; }
        [Column]
        public string TenderSalePlace { get; set; }
        [Column]
        public string Title { get; set; }
    }

    [Table]
    public class investmentAnnouncements
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }
        [Column]
        public int BuyByInternet { get; set; }
        [Column]
        public string BuyingStartDate { get; set; }
        [Column]
        public string InvelopsOpeningDate { get; set; }
        [Column]
        public string ModificationDate { get; set; }
        [Column]
        public string ReceivingEndDate { get; set; }
        [Column]
        public string ReceivingStartDate { get; set; }
        [Column]
        public string TenderDate { get; set; }
        [Column]
        public string TenderDescription { get; set; }
        [Column]
        public int TenderID { get; set; }
        [Column]
        public string TenderNumber { get; set; }
        [Column]
        public int TenderPrice { get; set; }
        [Column]
        public string TenderTitle { get; set; }
        [Column]
        public string displayedTitle { get; set; }
    }

    [Table]
    public class checkedNotifications
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }
        [Column]
        public string name { get; set; }
        [Column]
        public bool checkedItem { get; set; }
    }

    [Table]
    public class evaluations
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public long ID { get; set; }
        [Column]
        public bool IsSatisfied { get; set; }
        [Column]
        public long NationalID { get; set; }
        [Column]
        public string Reasons { get; set; }
        [Column]
        public string Suggestions { get; set; }
    }

    [Table]
    public class aboutNajran
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public string AboutApp { get; set; }
        [Column]
        public string ImageUrl { get; set; }
        [Column(DbType = "image", UpdateCheck = UpdateCheck.Never)]
        public byte[] image { get; set; }
    }

    //[Table]
    //public class poll
    //{
    //    [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
    //    public int id_pk { get; set; }
    //    [Column]
    //    public int ID { get; set; }
    //    [Column]
    //    public string Question { get; set; }
    //    [Column]
    //    public List<pollAnswers> QuestionAnswers { get; set; }
    //}

    [Table]
    public class pollTable
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int TableID { get; set; }
        [Column]
        public long ID { get; set; }
        [Column]
        public string Question { get; set; }
    }

    [Table]
    public class pollAnswersTable
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public string Answer { get; set; }
        [Column]
        public int Count { get; set; }
        [Column]
        public int ID { get; set; }
        [Column]
        public int QuestionID { get; set; }
        [Column]
        public int pollID { get; set; }
    }

    [Table]
    public class myRequests
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id { get; set; }
        [Column]
        public string Attachments { get; set; }
        [Column]
        public string Detail { get; set; }
        [Column]
        public string ERequestID { get; set; }
        [Column]
        public string NationalID { get; set; }
        [Column]
        public string RequestDate { get; set; }
        [Column]
        public string RequestStatus { get; set; }
        [Column]
        public int RequestTypeID { get; set; }
        [Column]
        public string RequestTypeName { get; set; }
        [Column]
        public string Title { get; set; }
    }

    [Table]
    public class emergencyGuid
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int id_pk { get; set; }
        [Column]
        public string Description { get; set; }
        [Column]
        public int ID { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public int Telephone { get; set; }
    }

    [Table]
    public class myTreatments
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int pk { get; set; }
        [Column]
        public string AttachmentCount { get; set; }
        [Column]
        public int ID { get; set; }
        [Column]
        public string LastDepartmentName { get; set; }
        [Column]
        public string LetterDate { get; set; }
        [Column]
        public string LetterNumber { get; set; }
        [Column]
        public long OperationFullNumber { get; set; }
        [Column]
        public string Subject { get; set; }
        [Column]
        public string displayedSubject { get; set; }
        [Column]
        public string TransactionDate { get; set; }
        [Column]
        public string TransactionType { get; set; }
    }

    [Table]
    public class Requests
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int ID { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public long NationalID { get; set; }
        [Column]
        public long PhoneNumber { get; set; }
        [Column]
        public long DistrictID { get; set; }
        [Column]
        public long RoadID { get; set; }
        [Column]
        public string Attachment { get; set; }
        [Column]
        public string Details { get; set; }
        [Column]
        public long ERequestID { get; set; }
        [Column]
        public string LocationDescription { get; set; }
        [Column]
        public string RequestDate { get; set; }
        [Column]
        public string RequestStatus { get; set; }
        [Column]
        public long RequestTypeID { get; set; }
        [Column]
        public string RequestTypeName { get; set; }
        [Column]
        public string Title { get; set; }


    }

    [Table]
    public class RegulationsData
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public long ID { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string displayedName { get; set; }
        [Column]
        public string Description { get; set; }
        [Column]
        public string displayedDiscription { get; set; }
        [Column]
        public string Link { get; set; }
        [Column]
        public string DownloadImgPath { get; set; }
        public string PdfImgPath { get; set; }

    }

    [Table]
    public class AddRequest
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public long DistrictID { get; set; }
        [Column]
        public double Latitude { get; set; }
        [Column]
        public string LocationDescription { get; set; }
        [Column]
        public double Longitude { get; set; }
        [Column]
        public long NationalID { get; set; }
        [Column]
        public string RequestDate { get; set; }
        [Column]
        public string RequestStatus { get; set; }
        [Column]
        public long RequestTypeID { get; set; }
        [Column]
        public long RoadID { get; set; }

    }

    [Table]
    public class ContactUs
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public string Address { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string FullName { get; set; }
        [Column]
        public string LetterDetails { get; set; }
        [Column]
        public string LetterTypeName { get; set; }
        [Column]
        public long MobileNo { get; set; }
        [Column]
        public long NationalID { get; set; }
        [Column]
        public string PObox { get; set; }
        [Column]
        public long TelephoneNo { get; set; }
        [Column]
        public string Title { get; set; }
        [Column]
        public byte[] attachments { get; set; }
    }

    [Table]
    public class Incidents
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public long CallerNumber { get; set; }
        [Column]
        public long DeptID { get; set; }
        [Column]
        public long DistrictID { get; set; }
        [Column]
        public string DistrictName { get; set; }
        [Column]
        public string IncidentDescription { get; set; }
        [Column]
        public double Latitude { get; set; }
        [Column]
        public string LocationDescription { get; set; }
        [Column]
        public double Longitude { get; set; }
        [Column]
        public long NationalID { get; set; }
        [Column]
        public long RoadID { get; set; }
        [Column]
        public string RoadName { get; set; }
        [Column]
        public long TypeID { get; set; }
    }

    [Table]
    public class LoginResponseTable
    {
        [Column(IsPrimaryKey = true, IsDbGenerated = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int TableID { get; set; }
        [Column]
        public string BankAccountNo { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string Fax { get; set; }
        [Column]
        public long IdentityNumber { get; set; }
        [Column]
        public bool IsLogin { get; set; }
        [Column]
        public string LoginMessage { get; set; }
        [Column]
        public string Mobile { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string POBox { get; set; }
        [Column]
        public long RegistrationTypeId { get; set; }
        [Column]
        public string Telephone { get; set; }
        [Column]
        public long UserProfileId { get; set; }
        [Column]
        public long ZipCode { get; set; }
    }

    [Table]
    public class loginData
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int idPK { get; set; }
        [Column]
        public bool IsLogin { get; set; }
        [Column]
        public string ErrorMessage { get; set; }
        [Column]
        public long RegistrationTypeId { get; set; }
        [Column]
        public string RegistrationType { get; set; }
        [Column]
        public string UserToken { get; set; }
    }

    [Table]
    public class individualUserData
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int idPK { get; set; }
        //[Column]
        //public DateTime? BirthDate { get; set; }
        [Column]
        public int BirthDateHijri { get; set; }
        [Column]
        public string BirthDateHijriString { get; set; }
        [Column]
        public string BirthDateString { get; set; }
        [Column]
        public string BirthPlace { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string ErrorMessage { get; set; }
        [Column]
        public string FamilyName { get; set; }
        [Column]
        public string Fax { get; set; }
        [Column]
        public string FirstName { get; set; }
        //[Column]
        //public DateTime? HefezaIssueDate { get; set; }
        [Column]
        public int HefezaIssueDateHijri { get; set; }
        [Column]
        public string HefezaIssueDateHijriString { get; set; }
        [Column]
        public string HefezaIssueSourcePlace { get; set; }
        [Column]
        public long HefezaIssueSourcePlaceId { get; set; }
        [Column]
        public string HefezaIssueString { get; set; }
        [Column]
        public long HefezaNumber { get; set; }
        [Column]
        public long IdentityNumber { get; set; }
        [Column]
        public int MaritalStatusId { get; set; }
        [Column]
        public string Mobile { get; set; }
        [Column]
        public string POBox { get; set; }
        [Column]
        public string SecondName { get; set; }
        [Column]
        public string Telephone { get; set; }
        [Column]
        public string ThirdName { get; set; }
        [Column]
        public long UserProfileId { get; set; }
        [Column]
        public long ZipCode { get; set; }
    }

    [Table]
    public class businessSectorData
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int idPK { get; set; }
        [Column]
        public string Address { get; set; }
        [Column]
        public string AttachmentReference { get; set; }
        [Column]
        public string BankAccountNo { get; set; }
        [Column]
        public long BankId { get; set; }
        //[Column]
        //public DateTime? CommercialRegDate { get; set; }
        [Column]
        public long CommercialRegDateHijri { get; set; }
        [Column]
        public long CommercialRegSourcePlaceId { get; set; }
        [Column]
        public string CommercialRegStringDate { get; set; }
        [Column]
        public string CommercialRegStringDateHijri { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string Fax { get; set; }
        [Column]
        public long IdentityBuisinessNumber { get; set; }
        [Column]
        public string LoginMessage { get; set; }
        [Column]
        public string Mobile { get; set; }
        [Column]
        public string Name { get; set; }
        [Column]
        public string POBox { get; set; }
        [Column]
        public string Telephone { get; set; }
        [Column]
        public long ZipCode { get; set; }
    }

    [Table]
    public class maritalStatus
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tabelID { get; set; }
        [Column]
        public string Code { get; set; }
        [Column]
        public int Id { get; set; }
        [Column]
        public string Title { get; set; }
    }

    [Table]
    public class banks
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public long BankID { get; set; }
        [Column]
        public string BankName { get; set; }
    }

    [Table]
    public class cities
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public string Code { get; set; }
        [Column]
        public int Id { get; set; }
        [Column]
        public string Title { get; set; }
    }

    [Table]
    public class citizenIndividualsData
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int idPK { get; set; }
        //[Column]
        //public DateTime? BirthDate { get; set; }
        [Column]
        public int BirthDateHijri { get; set; }
        [Column]
        public string BirthDateHijriString { get; set; }
        [Column]
        public string BirthDateString { get; set; }
        [Column]
        public string BirthPlace { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string ErrorMessage { get; set; }
        [Column]
        public string FamilyName { get; set; }
        [Column]
        public string Fax { get; set; }
        [Column]
        public string FirstName { get; set; }
        //[Column]
        //public DateTime? HefezaIssueDate { get; set; }
        [Column]
        public int HefezaIssueDateHijri { get; set; }
        [Column]
        public string HefezaIssueDateHijriString { get; set; }
        [Column]
        public string HefezaIssueSourcePlace { get; set; }
        [Column]
        public long HefezaIssueSourcePlaceId { get; set; }
        [Column]
        public string HefezaIssueString { get; set; }
        [Column]
        public long HefezaNumber { get; set; }
        [Column]
        public long IdentityNumber { get; set; }
        [Column]
        public int MaritalStatusId { get; set; }
        [Column]
        public string Mobile { get; set; }
        [Column]
        public string POBox { get; set; }
        [Column]
        public string SecondName { get; set; }
        [Column]
        public string Telephone { get; set; }
        [Column]
        public string ThirdName { get; set; }
        [Column]
        public long UserProfileId { get; set; }
        [Column]
        public long ZipCode { get; set; }
    }

    [Table]
    public class foreignIndividualsData
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int idPK { get; set; }
        //[Column]
        //public DateTime? BirthDate { get; set; }
        [Column]
        public int BirthDateHijri { get; set; }
        [Column]
        public string BirthDateHijriString { get; set; }
        [Column]
        public string BirthDateString { get; set; }
        [Column]
        public string BirthPlace { get; set; }
        [Column]
        public string Email { get; set; }
        [Column]
        public string ErrorMessage { get; set; }
        [Column]
        public string FamilyName { get; set; }
        [Column]
        public string Fax { get; set; }
        [Column]
        public string FirstName { get; set; }
        [Column]
        public long IdentityNumber { get; set; }
        [Column]
        public int MaritalStatusId { get; set; }
        [Column]
        public string Mobile { get; set; }
        [Column]
        public string POBox { get; set; }
        [Column]
        public string SecondName { get; set; }
        [Column]
        public string Telephone { get; set; }
        [Column]
        public string ThirdName { get; set; }
        [Column]
        public long UserProfileId { get; set; }
        [Column]
        public long ZipCode { get; set; }
    }

    [Table]
    public class postResult
    {
        [Column(IsDbGenerated = true, IsPrimaryKey = true, DbType = "INT NOT NULL Identity", CanBeNull = false, AutoSync = AutoSync.OnInsert)]
        public int tableID { get; set; }
        [Column]
        public int AffectedRows { get; set; }
        [Column]
        public int StatusCode { get; set; }
        [Column]
        public string StatusMessage { get; set; }
    }

}
