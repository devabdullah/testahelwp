﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Testahel_WP_v1._0
{
    public class pollAnswers
    {
        public string Answer { get; set; }
        public int Count { get; set; }
        public int ID { get; set; }
        public int QuestionID { get; set; }
    }
    public class poll
    {
        public int id_pk { get; set; }
        public int ID { get; set; }
        public string Question { get; set; }
        public List<pollAnswers> QuestionAnswers { get; set; }
    }
}
