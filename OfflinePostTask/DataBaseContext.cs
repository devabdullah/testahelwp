﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Linq;

namespace Testahel_WP_v1._0
{
    public class DataBaseContext : DataContext
    {
        public static string DBConnectionString = "Data Source=isostore:/testahelDB.sdf";
        public DataBaseContext(string conString)
            : base(conString)
        {

        }
        public Table<login> Login;
        public Table<requestTypes> RequestTypes;
        public Table<districts> Districts;
        public Table<allRoads> AllRoads;
        public Table<incidentCategories> IncidentCategories;
        public Table<allIncidentTypes> AllIncidentTypes;
        public Table<streets> Streets;
        public Table<incidentCategoriesTypes> IncidentCategoriesTypes;
        public Table<notifications> Notifications;
        public Table<tenderAnnouncements> TenderAnnouncements;
        public Table<investmentAnnouncements> InvestmentAnnouncements;
        public Table<checkedNotifications> CheckedNotifications;
        public Table<evaluations> Evaluations;
        public Table<aboutNajran> AboutNajran;
        public Table<pollAnswersTable> PollAnswersTable;
        public Table<myRequests> MyRequests;
        public Table<emergencyGuid> EmergencyGuid;
        public Table<myTreatments> MyTreatments;
        public Table<pollTable> PollTable;
        public Table<Requests> AllRequests;
        public Table<RegulationsData> AllRegulations;
        public Table<AddRequest> AllAddRequests;
        public Table<ContactUs> AllContactUs;
        public Table<Incidents> AllIncidents;
        public Table<login> AllLogin;
        public Table<LoginResponseTable> AllLoginResponses;
        public Table<loginData> LoginData;
        public Table<individualUserData> IndividualUserData;
        public Table<businessSectorData> BusinessSectorData;
        public Table<maritalStatus> MaritalStatus;
        public Table<banks> Banks;
        public Table<cities> Cities;
        public Table<citizenIndividualsData> CitizenIndividualsData;
        public Table<foreignIndividualsData> ForeignIndividualsData;
        public Table<postResult> PostResult;
    }
}
